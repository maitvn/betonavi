// using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

function callOnlineStatus () {
  setInterval(function () {
    $.post('/translatex/api/online_status', {url: location.href});
  }, 3000)
}

var statusOnlineUsers = [];

// function updateOnlineStatus () {
//   setInterval(function () {
//     $.get('/translatex/api/online_status', {url: location.href}, function (data) {
//       statusOnlineUsers = data.users;
//
//     });
//
//     statusOnlineUsers.push({
//       username: 'hihi',
//       pages: []
//     })
//   }, 3000)
// }



new Vue({
  el: '#onlineStatus',
  data: {
    onlineUsers: []
  },
  filters: {
  	truncate: function(string, value) {
    	return string.substring(0, value);
    }
  },
  mounted: function () {
    this.updateOnlineStatus();
  },
  methods: {
    updateOnlineStatus: function () {
      var self = this;
      $.get('/translatex/api/online_status', {url: location.href}, function (data) {
        self.onlineUsers = data.users;
      });

      setInterval(function () {
        $.get('/translatex/api/online_status', {url: location.href}, function (data) {
          self.onlineUsers = data.users;
        });
      }, 3000)
    }
  }
})



$(document).ready(function () {

  // call polling here
  callOnlineStatus();
  // updateOnlineStatus();

  $('#bell').click(function () {
    $('#notification').toggle();
  })

  $(window).click(function(event) {
    if (event.target !== $('#bell i').get(0)) {
      $('#notification').hide();
    }
  });

  $('#notification').click(function(event){
      event.stopPropagation();
  });

  $("time.timeago").timeago();

  /* List view filtering */
  $('#statusFilter, #assigneeFilter').change(function () {
    location.href = $(this).val();
  })
})
