from django.contrib import admin
from django.contrib.admin import AdminSite
from .models import *
from django.utils.html import format_html
from markdownx.widgets import AdminMarkdownxWidget
from django.utils.safestring import  mark_safe
from django.contrib import messages


@admin.register(Device)
class DeviceAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'device_uuid',
        'device_model',
        'device_country',
        'device_system_name',
        'app_version',
        'created_at',
    )
    # list_filter = ('parent', )
