from django.core.management.base import BaseCommand, CommandError

from webapp.models import *
from translatex.models import *


class Command(BaseCommand):
    help = 'Copy product from AmazonProduct to Translation'

    def handle(self, *args, **options):

        for item in AmazonProduct.objects.exclude(asin='').filter(asin__isnull=False):
            old_vn_name = ''
            old_word_count = 0
            if item.product:
                old_vn_name = item.product.vn_name
                intro_count = len(item.product.vn_introduction.split()) if item.product.vn_introduction else 0
                desc_count = len(item.product.vn_description.split()) if item.product.vn_description else 0
                old_word_count = intro_count + desc_count

            object, created = ProductTranslation.objects.update_or_create(
                amazon_product=item,
                defaults={
                    'old_vn_name': old_vn_name,
                    'old_word_count': old_word_count
                }
            )

        self.stdout.write(self.style.SUCCESS('Done!'))
