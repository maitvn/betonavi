# -*- coding: utf-8 -*-
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from serializers import *
from rest_framework import status
from .models import *
import urllib2, urllib
from django.core.files import File
import ntpath
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Sum
import json
from django.contrib import messages
from invitations.models import Invitation
import re


def validate_request(argument):
    def real_decorator(function):
        def wrapper(request, *args, **kwargs):
            serializer = argument(data=request.data)
            if serializer.is_valid():
                return function(request, *args, **kwargs)
            else:
                return JsonResponse(
                    serializer.errors,
                    status=status.HTTP_400_BAD_REQUEST)

        return wrapper
    return real_decorator


@csrf_exempt
@api_view(['DELETE', 'POST', 'PUT'])
@validate_request(CartSerializer)
def cart(request):
    if request.user.is_anonymous():
        return JsonResponse({
            'error': 'Authentication Failed',
            'message': 'Bạn cần là thành viên để sử dụng tính năng này!'
        }, status=400)

    flag = request.data.get('flag', True)
    product_id = request.data.get('product_id', None)
    cart_id = request.data.get('cart_id', None)
    quantity = request.data.get('quantity', 1)


    if request.method == 'POST':
        size = request.data.get('size', None)
        color = request.data.get('color', None)

        product = Product.objects.get(pk=product_id)
        if color:
            try:
                colors = json.loads(product.color_json)
                if any(x for x in colors if x['name'] == color):
                    pass
                else:
                    color = None
            except Exception as e:
                color = None

        if size:
            try:
                sizes = json.loads(product.size_json)
                if size in sizes:
                    pass
                else:
                    size = None
            except Exception:
                size = None

        if not Cart.objects.filter(
                user=request.user,
                product_id=product_id,
                size=size,
                color=color
            ).exists():
            Cart.objects.create(
                user=request.user,
                product_id=product_id,
                quantity=1,
                size=size,
                color=color
            )
    if request.method == 'PUT':
        item = Cart.objects.get(pk=cart_id)
        item.quantity = quantity
        item.save()
    if request.method == 'DELETE':
        # delete item
        item = Cart.objects.get(pk=cart_id)
        item.delete()

    # count again
    return JsonResponse({
        'message': 'success',
        'count': Cart.objects.filter(
            user=request.user
            ).aggregate(Sum('quantity'))['quantity__sum'] or 0
    })


@csrf_exempt
@api_view(['DELETE', 'POST'])
def favorite(request):
    if request.user.is_anonymous():
        return JsonResponse({
            'error': 'Authentication Failed',
            'message': 'Bạn cần là thành viên để sử dụng tính năng này!'
        }, status=400)

    saved_id = request.data.get('saved_id', None)
    product_id = request.data.get('product_id', None)

    if request.method == 'POST':
        if not Favorite.objects.filter(user=request.user, product_id=product_id).exists():
            Favorite.objects.create(user=request.user, product_id=product_id)
    if request.method == 'DELETE':
        item = Favorite.objects.get(pk=saved_id)
        item.delete()
    return JsonResponse({
        'message': 'success',
        'count': Favorite.objects.filter(user=request.user).count()
    })


@csrf_exempt
@api_view(['POST'])
def followEmail(request):
    email = request.data.get('email', None)

    if request.method == 'POST':
        RequestInvitation.objects.create(
            email=email
        )

    return JsonResponse({
        'message': 'success',
    })



@csrf_exempt
@api_view(['GET'])
def send_invite_email(request, id):
    request_invite = RequestInvitation.objects.get(pk=id)

    if not request_invite.is_send_invite:
        request_invite.is_send_invite = True
        request_invite.save()
        try:
            invite = Invitation.create(request_invite.email, inviter=request.user)
            invite.send_invitation(request)
        except Exception as e:
            pass

    return JsonResponse({
        'message': 'success',
    })


@csrf_exempt
@api_view(['POST'])
def requestProduct(request):
    email = request.data.get('email', None)
    content = request.data.get('content', None)

    if request.user.is_anonymous():
        user = None
    else:
        user = request.user
    if request.method == 'POST':
        if content:
            ProductRequest.objects.create(
                email=email,
                content=content,
                user=user
            )

    return JsonResponse({
        'message': 'success',
    })


@csrf_exempt
def import_from_amazon_ext(request):
    if not request.user.is_staff:
        return JsonResponse({
            'permission': False
        })

    asin = request.POST.get('asin')

    category_id = int(request.POST.get('category_id', None))
    if category_id == 0:
        category_id = None


    title = unicode(request.POST.get('jp_name', ''))
    vn_name = unicode(request.POST.get('vn_name', ''))
    vn_introduction = unicode(request.POST.get('vn_introduction', ''))
    vn_description = unicode(request.POST.get('vn_description', ''))
    detail_table = json.loads(unicode(request.POST.get('jp_detail_table', '{}')))

    jp_price = unicode(request.POST.get('jp_price', '')).replace(',', '')
    price_list = [int(s) for s in jp_price.split() if s.isdigit()]
    jp_price = price_list[0] if len(price_list) > 0 else 0

    weight = 0
    shipping_weight = 0
    raw_weight = None
    raw_shipping_weight = None
    raw_rating = ''
    raw_rank = ''
    for key in detail_table:
        if key in ['Shipping Weight', '商品重量']:
            raw_weight = detail_table[key]

        if key in ['Average Customer Review', 'おすすめ度']:
            raw_rating = detail_table[key]

        if key in ['Amazon Bestsellers Rank', 'Amazon 売れ筋ランキング']:
            raw_rank = detail_table[key]

        if key in [u'Item Weight', u'商品重量']:
            raw_weight = detail_table[key]

        if key in [u'Shipping Weight', u'発送重量']:
            raw_shipping_weight = detail_table[key]

    if raw_weight:
        matchObj = re.match(r'(\d+\.?[\d+]?).*Kg', raw_weight, re.U|re.S)
        if matchObj:
            weight = int(float(matchObj.group(1)) * 1000)
        else:
            matchObj = re.match(r'(\d+\.?[\d+]?).*g', raw_weight, re.U|re.S)
            if matchObj:
                weight = int(float(matchObj.group(1)) * 1)

    if raw_shipping_weight:
        matchObj = re.match(r'(\d+\.?[\d+]?).*Kg', raw_shipping_weight, re.U|re.S)
        if matchObj:
            shipping_weight = int(float(matchObj.group(1)) * 1000)
        else:
            matchObj = re.match(r'(\d+\.?[\d+]?).*g', raw_shipping_weight, re.U|re.S)
            if matchObj:
                shipping_weight = int(float(matchObj.group(1)) * 1)


    if asin and not AmazonProduct.objects.filter(asin=asin).exists():
        AmazonProduct.objects.create(
            asin=request.POST.get('asin'),
            price=jp_price,
            title=title,
            weight=weight,
            shipping_weight=shipping_weight,
            rating=raw_rating,
            rank=raw_rank,
        )

    if asin and Product.objects.filter(asin=asin).exists():
        product = Product.objects.get(asin=asin)
        product.vn_name = vn_name
        product.amazon_price = jp_price
        product.vn_introduction = vn_introduction
        product.vn_description = vn_description
        product.category_id = category_id
        product.jp_detail_table = json.dumps(detail_table)
        product.weight = weight
        product.shipping_weight = shipping_weight
        product.save()

        return JsonResponse({
            'update': True
        })
    else:
        product = Product.objects.create(
            asin=request.POST.get('asin'),
            amazon_price=jp_price,
            vn_name=vn_name,
            category_id=category_id,
            vn_introduction=vn_introduction,
            vn_description=vn_description,
            jp_detail_table=json.dumps(detail_table),
            vn_detail_table=json.dumps(detail_table),
            weight=weight,
            shipping_weight=shipping_weight,
            on_sale=False,
            added_by=request.user,
        )
        images = request.POST.get('images').split(',')
        for img in images:
            remote_image = urllib.urlretrieve(img)
            image = ProductImage.objects.create(
                product=product
            )
            image.image.save(
                ntpath.basename(img),
                File(open(remote_image[0]))
            )
            if not product.image:
                product.image = image.image
                product.save()

        return JsonResponse({
            'add': False
        })


@csrf_exempt
@api_view(['GET'])
def ext_get_category(request):
    category = CategorySerializer(
        Category.objects.all(),
        many=True
    ).data
    return Response({
        'categories': category
    })


@csrf_exempt
@api_view(['GET'])
def send_message_2_customer(request):

    message = request.GET.get('message')
    order_id = int(request.GET.get('order_id'))

    order = Order.objects.get(pk=order_id)

    OrderMessage.objects.create(
        order=order,
        user=order.user,
        staff=request.user,
        message=message,
        is_staff_reply=True,
        is_mail_user=False
    )

    return Response({
        # 'messagessss': 'message'
    })


@csrf_exempt
@api_view(['GET', 'POST'])
def ext_get_product(request):
    asin = request.GET.get('asin', None)
    product = {}
    if asin:
        try:
            product = ProductSerializer(
                Product.objects.get(asin=asin),
                many=False
            ).data
        except ObjectDoesNotExist:
            pass
    return Response({
        'product': product
    })
