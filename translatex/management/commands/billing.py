from django.core.management.base import BaseCommand, CommandError

from webapp.models import *
from translatex.models import *
from datetime import datetime


class Command(BaseCommand):
    help = 'Calculate Billing monthly'

    def handle(self, *args, **options):
        year = 2017
        month = 9

        # Calculate billing
        for user in Translator.objects.all():
            if Billing.objects.filter(translator=user, year=year, month=month).exists():
                self.stdout.write(self.style.ERROR('[ERROR]...%s bill already exists' % user.fullname))
                continue

            self.stdout.write(self.style.SUCCESS('[Calculate]...%s' % user.fullname))
            charge_amount = ProductTranslation.objects.filter(
                assignee=user,
                status=4,
                is_paid=False
            ).aggregate(Sum('charge_amount'))['charge_amount__sum']

            Billing.objects.create(
                translator=user,
                year=year,
                month=month,
                charge_amount=charge_amount,
            )
            ProductTranslation.objects.filter(
                assignee=user,
                status=4,
                is_paid=False
            ).update(is_paid=True)

        self.stdout.write(self.style.SUCCESS('Done!'))
