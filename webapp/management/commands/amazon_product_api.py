#AWSAccessKeyId=AKIAIXC5CY6IM4SMKY6A
#AWSSecretKey=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
#Tagname: ryu321-22

# docs: https://github.com/yoavaviram/python-amazon-simple-product-api
# '''
# actors
# api
# asin
# author
# authors
# availability
# availability_max_hours
# availability_min_hours
# availability_type
# aws_associate_tag
# binding
# brand
# browse_nodes
# color
# creators
# detail_page_url
# directors
# ean
# edition
# editorial_review
# editorial_reviews
# eisbn
# features
# formatted_price
# genre
# get_attribute
# get_attribute_details
# get_attributes
# get_parent
# images
# is_adult
# is_preorder
# isbn
# label
# languages
# large_image_url
# list_price
# manufacturer
# medium_image_url
# model
# mpn
# offer_id
# offer_url
# pages
# parent
# parent_asin
# parsed_response
# part_number
# price_and_currency
# product_group
# product_type_name
# publication_date
# publisher
# region
# release_date
# reviews
# running_time
# sales_rank
# sku
# small_image_url
# studio
# tiny_image_url
# title
# to_string
# upc
# '''
from amazon.api import AmazonAPI
from django.core.management.base import BaseCommand, CommandError
from webapp.models import *
import time
from datetime import datetime, timedelta
import json
from django.db.models import Q

class Command(BaseCommand):
    help = 'Amazon API '

    def handle(self, *args, **options):

        # Correct News and Feature uq_order
        self.stdout.write(self.style.NOTICE('[amazon api] Amazon API start ...'))

        for i in range(1000):

            try:
                time_threshold = datetime.now() - timedelta(days=1)

                asin_list = list(AmazonProduct.objects.filter(
                    Q(amazon_fetched_at__lt=time_threshold) |
                    Q(amazon_fetched_at__isnull=True)
                ).order_by('?').values_list('asin', flat=True)[:10])

                asin_list = ','.join(asin_list)

                print '[lookup] %s' % asin_list

                amazon = AmazonAPI(
                    settings.AMAZON_ACCESS_KEY,
                    settings.AMAZON_ACCESS_SECRET,
                    settings.AMAZON_AFFILIATE_ID,
                    region=settings.AMAZON_REGION)

                products = amazon.lookup(ItemId=asin_list)
                print 'Result: ', products
                now = datetime.now()
                for item in products:
                    price = None
                    try:
                        price = int(item.price_and_currency[0])
                    except Exception:
                        pass

                    images = []
                    try:
                        for img in item.images:
                            images.append(str(img.LargeImage.URL))
                    except Exception:
                        pass

                    reviews_url = ''
                    try:
                        reviews_url = item.reviews[1]
                    except Exception:
                        pass

                    amz_item = AmazonProduct.objects.get(asin=item.asin)

                    amz_item.title = item.title
                    amz_item.ean = item.ean
                    amz_item.brand = item.brand
                    amz_item.product_group = item.product_group
                    amz_item.product_type_name = item.product_type_name
                    amz_item.sales_rank = item.sales_rank
                    amz_item.large_image_url = item.large_image_url
                    amz_item.medium_image_url = item.medium_image_url
                    amz_item.price = price
                    amz_item.reviews_url = reviews_url
                    amz_item.images_list = json.dumps(images)
                    amz_item.features = json.dumps(item.features)
                    amz_item.publisher = item.publisher
                    amz_item.manufacturer = item.manufacturer

                    amz_item.amazon_fetched_at = now

                    amz_item.price_history = amz_item.price_history or '[]'

                    price_history = json.loads(amz_item.price_history or '[]')
                    price_history.append({
                        'date': now.strftime('%Y/%m/%d %H:%M:%S'),
                        'price': price
                    })
                    amz_item.price_history = json.dumps(price_history)
                    amz_item.save()

            except Exception as e:
                print str(e)
            time.sleep(10) # sleep 10s

        self.stdout.write(self.style.SUCCESS('Done!'))
