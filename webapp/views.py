from django.shortcuts import render, redirect
from django.http import Http404
from django.shortcuts import render_to_response
from django.template import RequestContext
from .models import *
from django.core.mail import send_mail
from .forms import *
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core import serializers
from django_user_agents.utils import get_user_agent
from django.http import HttpResponseRedirect
import random
import string
from invitations.models import Invitation
import json
from django.core.mail import mail_admins
from markdownx.utils import markdownify
import math
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
import uuid


def roundup(x):
    return int(math.ceil(x / 1000.0)) * 1000

def handler404(request):
    response = render_to_response('404.html', {},
                                  context_instance=RequestContext(request))
    response.status_code = 404
    return response


def handler500(request):
    response = render_to_response('500.html', {},
                                  context_instance=RequestContext(request))
    response.status_code = 500
    return response


def index(request):
    user_agent = get_user_agent(request)
    if user_agent.is_mobile or user_agent.is_tablet:
        return render(request, 'webapp/mobile/index.html', {
            'category': Category.objects.filter(parent__isnull=True, status=0),
            'banners': Banner.objects.all(),
            # 'banner': Banner.objects.first(),
        })

    return render(request, 'webapp/index.html', {
        'category': Category.objects.filter(parent__isnull=True, status=0),
        'banners': Banner.objects.all(),
        # 'banner': Banner.objects.first(),
    })


def request_invitation(request):
    if request.method == 'POST':
        form = RequestMemberShipForm(request.POST)
        if not form.is_valid():
            return render(request, 'account/request_invitation.html', {
                'error': True,
                'form': form
            })

        if User.objects.filter(email=request.POST.get('email')).exists():
            return render(request, 'account/request_invitation.html', {
                'case': 1,
                'done': True
            })
        elif RequestInvitation.objects.filter(email=request.POST.get('email')).exists():
            return render(request, 'account/request_invitation.html', {
                'case': 2,
                'done': True
            })
        else:
            RequestInvitation.objects.create(
                email=request.POST.get('email'),
                message=request.POST.get('message'),
            )
            return render(request, 'account/request_invitation.html', {
                'done': True,
                'case': 3,
            })

    return render(request, 'account/request_invitation.html', {
        'done': False,
        'form': RequestMemberShipForm()
    })


def landing(request):
    return render(request, 'webapp/landing.html', {})


def category(request, slug):
    category = Category.objects.get(slug=slug)

    product_list = Product.objects.filter(
        Q(category_id=category.id) | Q(
            Q(category__parent__isnull=False) &
            Q(category__parent__id=category.id)
        ) | Q(
            Q(category__parent__isnull=False) &
            Q(category__parent__parent__isnull=False) &
            Q(category__parent__parent__id=category.id)
        ),
        sale_price__isnull=False
    ).order_by('-score', '-id')

    paginator = Paginator(product_list, settings.PAGING_SIZE) # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        products = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        products = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        products = paginator.page(paginator.num_pages)

    user_agent = get_user_agent(request)
    if user_agent.is_mobile or user_agent.is_tablet:
        template = 'webapp/mobile/category.html'
    else:
        template = 'webapp/category.html'

    return render(request, template, {
        'category': category,
        'products': products,
        'child_categories': Category.objects.filter(parent=category)
    })


def search(request):
    keyword = request.GET['q'] if 'q' in request.GET else ''
    request.session['keyword'] = keyword

    product_list = Product.objects.filter(vn_name__search=keyword, sale_price__isnull=False).order_by()
    paginator = Paginator(product_list, settings.PAGING_SIZE) # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        products = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        products = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        products = paginator.page(paginator.num_pages)


    template = 'webapp/search.html'
    user_agent = get_user_agent(request)
    if user_agent.is_mobile or user_agent.is_tablet:
        template = 'webapp/mobile/search.html'

    return render(request, template, {
        'keyword': keyword,
        'products': products
    })


def dp_product(request, code):
    product = Product.objects.filter(
        Q(asin=code) | Q(ean=code)
    ).first()

    if product:
        return HttpResponseRedirect('/product/%s-%s' % (product.id, product.slug))
    else:
        raise Http404(u"Product not found!")

def product(request, product_id, slug):
    product = Product.objects.get(pk=product_id)
    product.view_count +=1
    product.save()

    technical_detail = {}
    try:
        technical_detail = json.loads(product.jp_detail_table).items
    except Exception:
        pass

    colors = {}
    try:
        colors = json.loads(product.color_json)
    except Exception:
        pass

    sizes = {}
    try:
        sizes = json.loads(product.size_json)
    except Exception:
        pass

    same_category_products = Product.objects.filter(
        Q(category_id=product.category.id),
        sale_price__isnull=False).order_by('?')[:6]

    template = 'webapp/product.html'
    user_agent = get_user_agent(request)
    if user_agent.is_mobile or user_agent.is_tablet:
        template = 'webapp/mobile/product.html'

    return render(request, template, {
        'detail': product,
        'detail_introduction': markdownify(product.vn_introduction) if product.vn_introduction else '',
        'detail_description': markdownify(product.vn_description) if product.vn_description else '',
        'technical_detail': technical_detail,
        'colors': colors,
        'sizes': sizes,
        'images': ProductImage.objects.filter(product=product),
        'same_category_products': same_category_products,
    })


def feature_list(request):
    user_agent = get_user_agent(request)
    if user_agent.is_mobile or user_agent.is_tablet:
        return render(request, 'webapp/mobile/feature_list.html', {
            'features': FeatureSet.objects.all()
        })

    return render(request, 'webapp/feature_list.html', {
        'features': FeatureSet.objects.all()
    })


def feature_set(request, slug):
    feature_set = FeatureSet.objects.get(slug=slug)
    user_agent = get_user_agent(request)
    if user_agent.is_mobile or user_agent.is_tablet:
        return render(request, 'webapp/mobile/feature_set.html', {
            'feature_set': feature_set,
            'related_set': FeatureSet.objects.all()[:5]
        })
    return render(request, 'webapp/feature_set.html', {
        'feature_set': feature_set,
        'related_set': FeatureSet.objects.all()[:5]
    })

@login_required
def cart(request):
    user_agent = get_user_agent(request)
    if user_agent.is_mobile or user_agent.is_tablet:
        return render(request, 'webapp/mobile/cart.html', {
            'products': Cart.objects.filter(user=request.user),
            'quantity_range': range(1, 11)
        })
    return render(request, 'webapp/cart.html', {
        'products': Cart.objects.filter(user=request.user),
        'quantity_range': range(1, 11)
    })



@login_required
def favorite(request):
    user_agent = get_user_agent(request)
    if user_agent.is_mobile or user_agent.is_tablet:
        return render(request, 'webapp/mobile/favorite.html', {
            'products': Favorite.objects.filter(user=request.user)
        })
    return render(request, 'webapp/favorite.html', {
        'products': Favorite.objects.filter(user=request.user)
    })


@login_required
def order_shipping_method(request):
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = ShippingAddressForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            shipping_address = ShippingAddress.objects.create(
                user=request.user,
                shipping_fullname=unicode(form.cleaned_data['shipping_fullname']),
                shipping_phone=form.cleaned_data['shipping_phone'],
                shipping_prefecture=unicode(form.cleaned_data['shipping_prefecture']),
                shipping_address1=unicode(form.cleaned_data['shipping_address1']),
                shipping_address2=unicode(form.cleaned_data['shipping_address2']),
            )
            return HttpResponseRedirect('/order/phuong-thuc-thanh-toan?k=%s' % uuid.uuid4())

    # if a GET (or any other method) we'll create a blank form
    else:
        form = ShippingAddressForm()

    user_agent = get_user_agent(request)
    template =  'webapp/order_shipping_method.html'
    if user_agent.is_mobile or user_agent.is_tablet:
        template = 'webapp/mobile/order_shipping_method.html'

    return render(request, template, {
        'form': form,
        'shipping_addresses': ShippingAddress.objects.filter(user=request.user)[:2],
        'k': uuid.uuid4()
    })



@login_required
def order_payment_method(request):
    user_agent = get_user_agent(request)
    template =  'webapp/order_payment_method.html'
    if user_agent.is_mobile or user_agent.is_tablet:
        template = 'webapp/mobile/order_payment_method.html'

    return render(request, template, {
        'bank_accounts': BankAccount.objects.all(),
        'k': uuid.uuid4() # fake string
    })


@login_required
def order_use_address(request):
    if request.method == 'POST':
        id = request.POST.get('id', None)
        address = ShippingAddress.objects.get(pk=int(id))
        address.save()
        return HttpResponseRedirect('/order/phuong-thuc-thanh-toan?k=%s' % uuid.uuid4())
    return HttpResponseRedirect('/cart')


@login_required
def order_confirmation(request):

    if request.method == 'GET':
        order_total = 0
        shipping_fee = 0
        for item in Cart.objects.filter(user=request.user):
            order_total += int(item.quantity * item.product.sale_price)
            weight = item.product.weight or 0
            shipping_fee += roundup((item.quantity * weight) * 25)

        shipping_fee = max(shipping_fee, 25000)

        price_off = 0
        price_off_reason = ''
        if order_total > settings.FIRST_ORDER_THREADHOLD and Order.objects.filter(user=request.user).count() == 0:
            price_off = settings.FIRST_ORDER_OFF
            price_off_reason = 'FIRST_ORDER'


        user_agent = get_user_agent(request)
        template =  'webapp/order_confirmation.html'
        if user_agent.is_mobile or user_agent.is_tablet:
            template = 'webapp/mobile/order_confirmation.html'
        return render(request, template, {
            'cart_items': Cart.objects.filter(user=request.user),
            'shipping_address': ShippingAddress.objects.filter(user=request.user).first(),
            'shipping_fee': shipping_fee,
            'order_total': order_total,
            'price_off': price_off,
            'price_off_reason': price_off_reason,
            'combined_total': order_total + shipping_fee - price_off
        })

    if request.method == 'POST':
        cart_item_count = Cart.objects.filter(user=request.user).count()
        if cart_item_count == 0:
            return HttpResponseRedirect('/cart')

        shipping_address = ShippingAddress.objects.filter(user=request.user).first()
        # Place the order
        order = Order.objects.create(
            user=request.user,
            order_number=''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(8)),
            shipping_fullname=shipping_address.shipping_fullname,
            shipping_phone=shipping_address.shipping_phone,
            shipping_prefecture=shipping_address.shipping_prefecture,
            shipping_address1=shipping_address.shipping_address1,
            shipping_address2=shipping_address.shipping_address2,
        )

        # Create Order item
        vn_total_price = 0
        jp_total_price = 0
        shipping_fee = 0
        total_weight = 0
        for item in Cart.objects.filter(user=request.user):
            orderItem = OrderItem.objects.create(
                order=order,
                product=item.product,
                quantity=item.quantity,
                color=item.color,
                size=item.size,
                vn_item_price=item.product.sale_price,
                jp_item_price=item.product.jp_price,
                vn_total_price=int(item.product.sale_price * item.quantity),
                jp_total_price=int(item.product.jp_price * item.quantity),
            )
            vn_total_price += int(item.product.sale_price * item.quantity)
            jp_total_price += int(item.product.jp_price * item.quantity)
            weight = item.product.weight or 0
            shipping_fee += roundup((item.quantity * weight) * 25)
            total_weight += weight

        shipping_fee = max(shipping_fee, 25000)

        price_off = 0
        if vn_total_price > settings.FIRST_ORDER_THREADHOLD and Order.objects.filter(user=request.user).count() == 1:
            price_off = settings.FIRST_ORDER_OFF

        order.vn_total_price = vn_total_price
        order.jp_total_price = jp_total_price
        order.shipping_fee = shipping_fee
        order.price_off = price_off
        order.total_weight = total_weight
        order.vn_order_amount = vn_total_price + shipping_fee - price_off
        order.save()

        # Delete in-card items
        Cart.objects.filter(user=request.user).delete()

        return HttpResponseRedirect('/order/hoan-tat')


@login_required
def order_complete(request):
    last_order = Order.objects.filter(user=request.user).order_by('-id').first()

    user_agent = get_user_agent(request)
    template =  'webapp/order_complete.html'
    if user_agent.is_mobile or user_agent.is_tablet:
        template = 'webapp/mobile/order_complete.html'

    # Sending email to customer & admin ....
    return render(request, template, {
        'order': last_order
    })


@login_required
def order_detail(request, uuid):
    order = Order.objects.get(uuid=uuid)
    user_agent = get_user_agent(request)
    template =  'webapp/order_detail.html'

    if request.method == 'POST':
        form = OrderMessageForm(request.POST)
        if form.is_valid():
            OrderMessage.objects.create(
                user=request.user,
                order=order,
                message=unicode(form.cleaned_data['message']),
            )

            # send mail to admins
            mail_admins(
                '[OrderMessage] %s chat about %s' % (
                    request.user.username, order.order_number),
                '''
                Message: %s
                ''' % unicode(form.cleaned_data['message'])
            )

        else:
            print 'Form is not valid'

    if user_agent.is_mobile or user_agent.is_tablet:
        template = 'webapp/mobile/order_detail.html'

    return render(request, template, {
        'order': order,
        'bank_accounts': BankAccount.objects.all(),
        'messages': OrderMessage.objects.filter(order=order),
        'form': OrderMessageForm(),
    })


@login_required
def profile(request):
    user_agent = get_user_agent(request)
    template =  'webapp/profile.html'
    if user_agent.is_mobile or user_agent.is_tablet:
        template = 'webapp/mobile/profile.html'

    return render(request, template, {
        'user': request.user,
        'profile': None,
        'addresses': ShippingAddress.objects.filter(user=request.user),
        'orders': Order.objects.filter(user=request.user),
        'invited_count': Invitation.objects.filter(inviter=request.user).count()
    })




def about_about(request):
    user_agent = get_user_agent(request)
    if user_agent.is_mobile or user_agent.is_tablet:
        return render(request, 'webapp/mobile/about_about.html', {})
    return render(request, 'webapp/about_about.html', {})


def about_term(request):
    user_agent = get_user_agent(request)
    if user_agent.is_mobile or user_agent.is_tablet:
        return render(request, 'webapp/mobile/about_term.html', {})
    return render(request, 'webapp/about_term.html', {})


def about_membership(request):
    user_agent = get_user_agent(request)
    if user_agent.is_mobile or user_agent.is_tablet:
        return render(request, 'webapp/mobile/about_membership.html', {
            'coupons': Coupon.objects.filter(is_invitation_coupon=True)
        })
    return render(request, 'webapp/about_membership.html', {
        'coupons': Coupon.objects.filter(is_invitation_coupon=True)
    })


def about_order(request):
    user_agent = get_user_agent(request)
    if user_agent.is_mobile or user_agent.is_tablet:
        return render(request, 'webapp/mobile/about_order.html', {})
    return render(request, 'webapp/about_order.html', {})


def about_faq(request):
    user_agent = get_user_agent(request)
    if user_agent.is_mobile or user_agent.is_tablet:
        return render(request, 'webapp/mobile/about_faq.html', {})
    return render(request, 'webapp/about_faq.html', {})


def about_contact(request):
    user_agent = get_user_agent(request)
    contact_sent = False

    if request.method == 'POST':
        form = ContactForm(data=request.POST)
        if form.is_valid():
            email = unicode(request.POST.get('email'))
            name = unicode(request.POST.get('name'))
            message = unicode(request.POST.get('message'))

            mail_admins(
                '[Contact] Customer contacted',
                '''
Email: %s
Name: %s

Message: %s

                ''' % (email, name, message)
            )
            contact_sent = True


    if user_agent.is_mobile or user_agent.is_tablet:
        return render(request, 'webapp/mobile/about_contact.html', {
            'form': ContactForm(),
            'contact_sent': contact_sent
        })
    return render(request, 'webapp/about_contact.html', {
        'form': ContactForm(),
        'contact_sent': contact_sent
    })

def about_recruit(request):
    user_agent = get_user_agent(request)
    if user_agent.is_mobile or user_agent.is_tablet:
        return render(request, 'webapp/mobile/about_recruit.html', {
            'form': ContactForm()
        })
    return render(request, 'webapp/about_recruit.html', {
        'form': ContactForm()
    })


def about_request_products(request):
    request_sent = False
    if request.method == 'POST':
        form = ProductRequestForm(data=request.POST)
        if form.is_valid():
            email = unicode(request.POST.get('email'))
            content = unicode(request.POST.get('content'))
            ProductRequest.objects.create(
                email=email,
                content=content,
                user=request.user
            )
            request_sent = True
    else:
        form = ProductRequestForm()

    user_agent = get_user_agent(request)
    if user_agent.is_mobile or user_agent.is_tablet:
        return render(request, 'webapp/mobile/about_request_product.html', {
            'form': form,
            'request_sent': request_sent
        })
    return render(request, 'webapp/about_request_product.html', {
        'form': form,
        'request_sent': request_sent
    })



def mail_magazine(request):
    mail = MailMagazine.objects.first()
    return render(request, 'webapp/email/magazine.html', {
        'products': Product.objects.all()[:5],
        'categories': mail.categories if mail else None,
        'host': settings.HOST,
    })
