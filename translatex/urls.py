from django.conf.urls import url
import views


urlpatterns = (

  # Get shop list
  url(r'^$', views.index, name="translatex_list"),
  url(r'^guide$', views.guide),
  url(r'^billings$', views.billing),
  url(r'^members$', views.members),
  url(r'^all_products$', views.all_products),
  url(r'^sp/(?P<id>[0-9]+)$', views.detail, name="translatex_detail"),

  # online status api
  url(r'^api/online_status$', views.online_status),
)
