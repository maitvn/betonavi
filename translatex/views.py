# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import messages
from django.shortcuts import render, redirect
from .models import *
from .forms import *
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .decorators import notification, permission
from django.contrib.auth.decorators import login_required
from datetime import datetime
from .mail import *
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django_user_agents.utils import get_user_agent
from markdownx.utils import markdownify
from .constants import *
from django.core.cache import cache
from django.contrib.auth.models import User
from django.http import JsonResponse


@login_required
@notification
@permission
def index(request):
    try:
        status = int(request.GET.get('status', None))
    except Exception as e:
        status = None

    if request.user.is_staff:
        product_query = ProductTranslation.objects
        if isinstance(status, int):
            product_query = product_query.filter(
                status=status
            )
        if request.GET.get('u', None):
            product_query = product_query.filter(
                assignee__user__username=request.GET.get('u')
            )
        product_query = product_query.order_by('-id')
    else:
        if isinstance(status, int):
            product_query = ProductTranslation.objects.filter(
                status=status,
                assignee__user=request.user
            ).order_by('-id')
        else:
            product_query = ProductTranslation.objects.filter(
                assignee__user=request.user
            ).order_by('-id')

    paginator = Paginator(product_query, settings.PAGING_SIZE) # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        products = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        products = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        products = paginator.page(paginator.num_pages)

    user_agent = get_user_agent(request)
    template_file = 'translatex/mobile/index.html' if user_agent.is_mobile else 'translatex/index.html'
    return render(request, template_file, {
        'products': products,
        'tab': 1,
        'status': TICKET_STATUS_OPTIONS,
        'translators': Translator.objects.all().select_related('user'),
        'filtered_status': status,
        'maintenance': settings.PTP_MAINTENANCE,
    })


@login_required
@notification
@permission
def detail(request, id):
    product = ProductTranslation.objects.get(pk=id)
    old_status = product.status
    old_assignee = product.assignee
    curr_url = reverse('translatex_detail', args=[id])

    if request.method == 'POST':
        form_type =request.POST.get('type')
        if form_type == 'status':
            # save status
            form = TicketStatusForm(data=request.POST)
            if form.is_valid():
                product.status = int(form.cleaned_data['status'])
                product.save()
                messages.success(request, 'Lưu trạng thái thành công! ')
                Notification.objects.create(
                    product=product,
                    user=request.user,
                    message=u'#%s [%s...]: trạng thái được cập nhật bởi %s ' % (
                        product.id,
                        product.amazon_product.title[:10],
                        request.user.username
                    )
                )

                if old_status != product.status:
                    mail_status_change(product, request.user, old_status)

                # redirect the same page
                return HttpResponseRedirect(curr_url)

        elif form_type == 'review':
            # save status
            form = ReviewForm(data=request.POST, instance=product)

            if form.is_valid():
                form.save()
                messages.success(request, 'Lưu trạng thái thành công! ')
                Notification.objects.create(
                    product=product,
                    user=request.user,
                    message=u'#%s [%s...]: trạng thái được cập nhật bởi %s ' % (
                        product.id,
                        product.amazon_product.title[:10],
                        request.user.username
                    )
                )

                if old_status != int(product.status):
                    mail_status_change(product, request.user, old_status)

                    if product.status == 4:
                        ## if closed save closed time
                        product.closed_at = datetime.now()
                        product.save()

                if product.assignee and old_assignee != product.assignee:
                    mail_change_assignee(product, request.user)

                # redirect the same page
                return HttpResponseRedirect(curr_url)

        elif form_type == 'content':
            # save status
            form = ContentForm(data=request.POST, instance=product)
            if form.is_valid():
                form.save()

                messages.success(request, 'Lưu bài dịch thành công! ')
                Notification.objects.create(
                    product=product,
                    user=request.user,
                    message=u'#%s [%s...]: bài dịch được cập nhật bởi %s ' % (
                        product.id,
                        product.amazon_product.title[:10],
                        request.user.username
                    )
                )

                # redirect the same page
                return HttpResponseRedirect(curr_url)

        elif form_type == 'edit':
            # save detail information
            form = EditTicketForm(data=request.POST, instance=product)
            if form.is_valid():
                form.save()
                messages.success(request, 'Lưu thông tin thành công! ')

                # redirect the same page
                return HttpResponseRedirect(curr_url)

        elif form_type == 'comment':
            # save comment
            form = TranslationCommentForm(data=request.POST)
            if form.is_valid():
                text = form.cleaned_data['message']
                TranslationComment.objects.create(
                    product=product,
                    user=request.user,
                    text=text
                )
                Notification.objects.create(
                    product=product,
                    user=request.user,
                    message=u'#%s [%s...]: nhận xét mới bởi %s ' % (
                        product.id,
                        product.amazon_product.title[:10],
                        request.user.username
                    )
                )

                # mail comment
                mail_comment(product, request.user, text)

                # redirect the same page
                return HttpResponseRedirect(curr_url)

    user_agent = get_user_agent(request)
    template_file = 'translatex/mobile/detail.html' if user_agent.is_mobile else 'translatex/detail.html'
    return render(request, template_file, {
        'product': product,
        'comments': TranslationComment.objects.filter(product=product).all(),
        'status_form': TicketStatusForm(initial={"status": product.status}),
        'review_form': ReviewForm(instance=product),
        'content_form': ContentForm(instance=product),
        'edit_form': EditTicketForm(instance=product),
        'comment_form': TranslationCommentForm(),
        'md_introduction': markdownify(product.vn_introduction) if product.vn_introduction else '',
        'md_description': markdownify(product.vn_description) if product.vn_description else '',
        'md_usage': markdownify(product.vn_usage) if product.vn_usage else '',
        'md_notice': markdownify(product.vn_notice) if product.vn_notice else '',
        'maintenance': settings.PTP_MAINTENANCE,
    })


@login_required
@notification
@permission
def guide(request):
    return render(request, 'translatex/guide.html', {
        'tab': 2
    })


@login_required
@notification
@permission
def billing(request):
    now = datetime.now()

    last_month_year = now.year if now.month > 1 else now.year - 1
    last_month = now.month - 1 if now.month > 1 else 12

    months = []
    for item in Billing.objects.all().order_by('-year', '-month'):
        ym = (item.year, item.month, )
        if ym not in months:
            months.append(ym)

    history = []

    if request.user.is_staff:
        translators = Translator.objects.all()
        for item in months:
            history.append(
                Billing.objects.filter(year=item[0], month=item[1]).order_by('-charge_amount')
            )

    else:
        translators = Translator.objects.filter(user=request.user)

        for item in months:
            history.append(Billing.objects.filter(
                translator__user=request.user,
                year=item[0],
                month=item[1]).order_by('-charge_amount'))

    user_agent = get_user_agent(request)
    template_file = 'translatex/mobile/billing.html' if user_agent.is_mobile else 'translatex/billing.html'
    return render(request, template_file, {
        'year': now.year,
        'month': now.month,
        'last_month_year': last_month_year,
        'last_month': last_month,
        'translators': translators,
        'history': history,
        'tab': 3
    })


@login_required
@notification
@permission
def members(request):
    if request.method == 'POST':
        form = TranslatorForm(request.POST)
        if form.is_valid():
            form.save()

    return render(request, 'translatex/member.html', {
        'tab': 4,
        'translators': Translator.objects.all(),
        'form': TranslatorForm()
    })



@login_required
@notification
@permission
def all_products(request):
    product_query = ProductTranslation.objects.all()

    paginator = Paginator(product_query, 500) # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        products = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        products = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        products = paginator.page(paginator.num_pages)


    return render(request, 'translatex/all_products.html', {
        'products': products,
        'tab': 1
    })


@login_required
@permission
def online_status(request):
    if request.method == 'POST':
        url = request.POST.get('url', None)
        status = cache.get('online_status:%s' % request.user.username)
        if status and url not in status:
            status.append(url)
        else:
            status = [url]

        cache.set('online_status:%s' % request.user.username, status, 10) # cache for 5 seconds

    elif request.method == 'GET' and request.user.is_staff:
        translators = [u.user.username for u in Translator.objects.all()]
        staffs = [u.username for u in User.objects.filter(is_staff=True)]
        users = translators + staffs

        data = []
        for user in users:
            status = cache.get('online_status:%s' % user)
            if status and user != request.user.username:
                data.append({
                    'abbr': user[:2].upper(),
                    'username': user,
                    'pages': status
                })
        return JsonResponse({
            'users': data
        })
    return JsonResponse({
    })
