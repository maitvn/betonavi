from django.core.management.base import BaseCommand, CommandError
# from webapp.models import *
from amazon.api import AmazonAPI

class Command(BaseCommand):
    help = 'Correct DB after run migration'

    def handle(self, *args, **options):

        # Correct News and Feature uq_order

        self.stdout.write(self.style.NOTICE('[stf_news] Update uq_order...'))


        self.stdout.write(self.style.NOTICE('[stf_feature] Update uq_order...'))


        self.stdout.write(self.style.NOTICE('[stf_device] Update firebase_token_cd from token_cd ...'))


        self.stdout.write(self.style.NOTICE('[stf_device] Update notification_flag ...'))


        self.stdout.write(self.style.SUCCESS('Done!'))
