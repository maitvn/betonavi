# -*- coding: utf-8 -*-
from rest_framework import serializers
from webapp.models import *
from .models import *
import json

class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = ('id', 'name', 'image')


class ProductSerializer(serializers.ModelSerializer):
    category = CategorySerializer()

    class Meta:
        model = Product
        fields = (
            'id',
            'slug',
            'asin',
            'ean',
            'sale_price',
            'price_off',
            'vn_name',
            'image',
            'vn_introduction',
            'vn_description',
            'category',
            'jp_detail_table',
        )


class AmazonProductSerializer(serializers.ModelSerializer):
    product = ProductSerializer()

    name = serializers.SerializerMethodField()
    def get_name(self, obj):
        if obj.product and obj.product.vn_name:
            return obj.product.vn_name
        return obj.title

    vn_price = serializers.SerializerMethodField()
    def get_vn_price(self, obj):
        if obj.product:
            return obj.product.sale_price
        else:
            return int(obj.price * 250)

    amazon_price = serializers.SerializerMethodField()
    def get_amazon_price(self, obj):
        return obj.price

    category = serializers.SerializerMethodField()
    def get_category(self, obj):
        if obj.product and obj.product.category:
            return obj.product.category.name
        return obj.product_group

    image_list = serializers.SerializerMethodField()
    def get_image_list(self, obj):
        try:
            return json.loads(obj.images_list.replace("'", '"'))
        except Exception:
            return []

    features = serializers.SerializerMethodField()
    def get_features(self, obj):
        try:
            return json.loads(obj.features)
        except Exception:
            return []

    class Meta:
        model = AmazonProduct

        exclude = ('status',)


class AmazonListProductSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()
    def get_name(self, obj):
        if obj.product and obj.product.vn_name:
            return obj.product.vn_name
        return obj.title

    vn_price = serializers.SerializerMethodField()
    def get_vn_price(self, obj):
        if obj.product:
            return obj.product.sale_price
        else:
            return int(obj.price * 250)

    amazon_price = serializers.SerializerMethodField()
    def get_amazon_price(self, obj):
        return obj.price

    category = serializers.SerializerMethodField()
    def get_category(self, obj):
        if obj.product and obj.product.category:
            return obj.product.category.name
        return obj.product_group

    class Meta:
        model = AmazonProduct

        fields = (
            'id',
            'medium_image_url',
            'ean',
            'name',
            'category',
            'vn_price',
            'amazon_price'
        )



######################
# REQUEST SERIALIZER #
######################
class RequestSerializer(serializers.Serializer):
    def update(self, instance, validated_data):
        pass
    def create(self, validated_data):
        pass
    device_uuid = serializers.CharField(max_length=200, required=True)
    client_key = serializers.CharField(max_length=200, required=True)

    device_manufacturer = serializers.CharField(max_length=200, required=False)
    device_name = serializers.CharField(max_length=200, required=False)
    device_model = serializers.CharField(max_length=200, required=False)
    device_locale = serializers.CharField(max_length=200, required=False)
    device_country = serializers.CharField(max_length=200, required=False)
    user_agent = serializers.CharField(max_length=2000, required=False)
    device_system_name = serializers.CharField(max_length=200, required=False)
    device_id = serializers.CharField(max_length=200, required=False)
    device_version = serializers.CharField(max_length=200, required=False)
    bundle_id = serializers.CharField(max_length=200, required=False)
    build_number = serializers.CharField(max_length=200, required=False)
    app_version = serializers.CharField(max_length=200, required=False)
    app_version_readable = serializers.CharField(max_length=200, required=False)
