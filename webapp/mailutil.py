# -*- coding: utf-8 -*-
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.conf import settings
from .models import *
from datetime import datetime
from django.contrib.auth.models import User


class EmailUtil:
    def __send(self, template_name, subject, params, mail_to):
        plaintext = get_template('email/%s.txt' % template_name)
        htmly     = get_template('email/%s.html' % template_name)
        text_content = plaintext.render(params)
        html_content = htmly.render(params)
        msg = EmailMultiAlternatives(
            subject,
            text_content,
            settings.ORDER_FROM_EMAIL,
            [mail_to],
            bcc=settings.ADMIN_EMAILS
        )
        msg.attach_alternative(html_content, "text/html")
        msg.send()

    def __sendHtml(self, template_name, subject, params, mail_to, html_params):
        plaintext = get_template('email/%s.txt' % template_name)
        htmly     = get_template('email/%s.html' % template_name)
        text_content = plaintext.render(params)
        html_content = htmly.render(html_params or params)
        msg = EmailMultiAlternatives(
            subject,
            text_content,
            settings.ORDER_FROM_EMAIL,
            [mail_to],
            bcc=settings.ADMIN_EMAILS
        )
        msg.attach_alternative(html_content, "text/html")
        msg.send()


    def send_order_confirm(self, params, mail_to):
        subject = u'[Betonavi.com] Xác nhận đặt hàng thành công'
        self.__send(
            template_name='order_confirm',
            subject=u'Xác nhận đặt hàng thành công',
            mail_to=mail_to,
            params=params
        )

    def send_order_delivering(self, params, mail_to):
        self.__send(
            template_name='order_delivering',
            subject=u'[Betonavi.com] Xác nhận Xuất hàng',
            mail_to=mail_to,
            params=params
        )

    def send_order_complete(self, params, mail_to):
        self.__send(
            template_name='order_complete',
            subject=u'[Betonavi.com] Đơn hàng đã hoàn tất',
            mail_to=mail_to,
            params=params
        )

    def send_order_message(self, params, mail_to):
        self.__send(
            template_name='order_message',
            subject=u'[Betonavi.com] Tin nhắn đơn hàng',
            mail_to=mail_to,
            params=params
        )

    def send_reply_invite_request(self, params,html_params, mail_to):
        self.__sendHtml(
            template_name='invite_request_reply',
            subject=u'[Betonavi.com] Chương trình thành viên Betonavi',
            mail_to=mail_to,
            params=params,
            html_params=html_params
        )

    def mail_magazine(self):
        mail = MailMagazine.objects.filter(
            scheduled_at__lte=datetime.now(),
            is_processed=False,
        ).first()

        if mail:
            mail.is_processed = True
            mail.save()

            mail_list = list(RequestInvitation.objects.all().values_list('email', flat=True)) + \
                list(User.objects.all().values_list('email', flat=True))

            # mail_list = list(set(mail_list))
            mail_list = settings.ADMIN_EMAILS

            plaintext = get_template('webapp/email/magazine.txt')
            htmly     = get_template('webapp/email/magazine.html')
            text_content = plaintext.render({
                'products': Product.objects.all().order_by('?')[:6],
                'categories': mail.categories if mail else None,
                'host': settings.HOST,
            })
            html_content = htmly.render({
                'products': Product.objects.all().order_by('?')[:6],
                'categories': mail.categories if mail else None,
                'host': settings.HOST,
            })
            msg = EmailMultiAlternatives(
                mail.subject,
                text_content,
                settings.MAGAZINE_FROM_EMAIL,
                mail_list,
                bcc=settings.ADMIN_EMAILS
            )
            msg.attach_alternative(html_content, "text/html")
            msg.send()
