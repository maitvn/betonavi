# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from webapp.models import *
import json, re

COST_PER_GRAM = 1 # 1 yen per 1 gram
EXCHANCE_RATE = 200

class Command(BaseCommand):
    help = 'Correct DB after run migration'

    def handle(self, *args, **options):
        #
        # # s = '304 g'
        # s = '1.2 Kg'
        # m = re.match(r'(\d+\.?[\d+]?).*Kg', s, re.U)
        #
        # print m
        # if m:
        #     print m.group()
        #     print m.group(0)
        #     print int(float(m.group(1)))
        #

        for item in Product.objects.filter(
            weight__isnull=True
        ):
            print u'....%s' % item.vn_name
            # print item.id

            try:
                detail_table = json.loads(item.jp_detail_table)

                for key in detail_table:
                    # print key, type(key)


                    if key in [u'Shipping Weight', u'発送重量']:
                        # g, Kg
                        item.raw_weight = detail_table[key].strip()

                        matchObj = re.match(r'(\d+\.?[\d+]?).*Kg', item.raw_weight, re.U|re.S)
                        print item.raw_weight
                        if matchObj:
                            item.weight = int(float(matchObj.group(1)) * 1000)
                        else:
                            # print 'w: |%s|' % item.raw_weight
                            matchObj = re.match(r'(\d+\.?[\d+]?).*g', item.raw_weight, re.U|re.S)
                            if matchObj:
                                item.weight = int(float(matchObj.group(1)) * 1)
                            else:
                                print 'www: |%s|' % item.raw_weight

                    if key in [u'Average Customer Review', u'おすすめ度']:
                        # 5つ星のうち 4.2

                        item.raw_rating = detail_table[key].strip().replace('\r', ' ').replace('\n', ' ')

                        matchObj = re.match(r'.*(\d\.\d).*', item.raw_rating, re.U|re.S)
                        if matchObj:

                            item.rating = float(matchObj.group(1))
                        else:
                            print 'r:', item.raw_rating

                    if key in [u'Amazon Bestsellers Rank', u'Amazon 売れ筋ランキング']:
                        item.raw_rank = detail_table[key]

                item.save()

            except Exception as e:
                pass
                print str(e)


        self.stdout.write(self.style.NOTICE('[batch] Update price ...'))
