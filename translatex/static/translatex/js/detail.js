

$(document).ready(function () {
  // loadpage
  var converter = new showdown.Converter();

  $('#pv_vn_name').text($('#id_vn_name').val());
  $('#pv_vn_introduction').html(converter.makeHtml($('#id_vn_introduction').val()));
  $('#pv_vn_description').html(converter.makeHtml($('#id_vn_description').val()));
  $('#pv_vn_usage').html(converter.makeHtml($('#id_vn_usage').val()));
  $('#pv_vn_notice').html(converter.makeHtml($('#id_vn_notice').val()));



  // event bindings
  $('#id_vn_name').bind('input propertychange', function() {
    $('#pv_vn_name').text($(this).val());
  });

  $('#id_vn_introduction').bind('input propertychange', function() {
    var converter = new showdown.Converter();
    var html = converter.makeHtml($(this).val());
    $('#pv_vn_introduction').html(html);
  });

  $('#id_vn_description').bind('input propertychange', function() {
    var converter = new showdown.Converter();
    var html = converter.makeHtml($(this).val());
    $('#pv_vn_description').html(html);
  });

  $('#id_vn_usage').bind('input propertychange', function() {
    var converter = new showdown.Converter();
    var html = converter.makeHtml($(this).val());
    $('#pv_vn_usage').html(html);
  });

  $('#id_vn_notice').bind('input propertychange', function() {
    var converter = new showdown.Converter();
    var html = converter.makeHtml($(this).val());
    $('#pv_vn_notice').html(html);
  });

})
