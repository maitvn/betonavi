from django.conf.urls import url
import views
import apis


urlpatterns = (

  # Get shop list
  url(r'^$', views.index),

  url(r'^landing$', views.landing),
  # url(r'^category/(?P<category_id>[0-9]+)$', views.category),
  url(r'^category/(?P<slug>[-\w]+)/$', views.category),
  # url(r'^product/(?P<product_id>[0-9]+)$', views.product),
  url(r'^product/(?P<product_id>[0-9]+)-(?P<slug>[-\w]+)/$', views.product),

  url(r'^dp/(?P<code>.*)$', views.dp_product),

  url(r'^search$', views.search),
  url(r'^favorite$', views.favorite),
  url(r'^cart$', views.cart),

  url(r'^private/mail-magazine$', views.mail_magazine),

  url(r'^features$', views.feature_list),
  url(r'^features/(?P<slug>[-\w]+)/$', views.feature_set),

  url(r'^order/phuong-thuc-ship$', views.order_shipping_method),
  url(r'^order/xac-nhan$', views.order_confirmation),
  url(r'^order/hoan-tat$', views.order_complete),
  url(r'^order/phuong-thuc-thanh-toan$', views.order_payment_method),
  url(r'^order/use-address$', views.order_use_address),
  url(r'^order/detail/(?P<uuid>.+)$', views.order_detail),


  url(r'^accounts/profile', views.profile),

  url(r'^request-an-invitation$', views.request_invitation),


  url(r'^api/request_product$', apis.requestProduct),
  url(r'^api/follow_email$', apis.followEmail),
  url(r'^api/cart$', apis.cart, name='add_remove_cart'),
  url(r'^api/favorite$', apis.favorite, name='add_remove_favorite'),
  url(r'^api/send_message_2_customer$', apis.send_message_2_customer, name='send_message_2_customer'),
  url(r'^api/send_invite_email/(?P<id>[0-9]+)$', apis.send_invite_email, name='send_invite_email'),


  url(r'^ext/api/import_from_amazon$', apis.import_from_amazon_ext, name='import_from_amazon_ext'),
  url(r'^ext/api/categories$', apis.ext_get_category, name='ext_get_category'),
  url(r'^ext/api/product$', apis.ext_get_product, name='ext_get_product'),

  # url(r'^about$', views.about),
  url(r'^gioi-thieu', views.about_about),
  url(r'^dieu-khoan', views.about_term),
  url(r'^chuong-trinh-thanh-vien', views.about_membership),
  url(r'^mua-hang-thanh-toan', views.about_order),
  url(r'^cau-hoi-thuong-gap', views.about_faq),
  url(r'^lien-he', views.about_contact),
  url(r'^tuyen-dung', views.about_recruit),
  # url(r'^yeu-cau-san-pham', views.about_request_products),
)
