

$(document).ready(function () {
  // loadpage
  var converter = new showdown.Converter();

  $('.editor').each(function () {
    var text = $(this).val();
    var html = converter.makeHtml(text);
    $(this).siblings('.preview').html(html);
  })

  // event bindings
  $('.editor').bind('input propertychange', function() {
    var text = $(this).val();
    var html = converter.makeHtml(text);

    $(this).siblings('.preview').html(html);
  });
})
