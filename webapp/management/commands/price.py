from django.core.management.base import BaseCommand, CommandError
from webapp.models import *
import math


def roundup(x):
    return int(math.ceil(x / 1000.0)) * 1000


class Command(BaseCommand):
    help = 'Correct DB after run migration'

    def handle(self, *args, **options):
        price_table = PriceTable.objects.values_list('min_price', 'max_price', 'rate')

        # Correct News and Feature uq_order
        for item in AmazonProduct.objects.all():
            product = Product.objects.filter(asin=item.asin).first()

            if product:
                print 'Updating ... %s' % product.vn_name
                # hard code for first time
                weight = max(product.shipping_weight or 0, product.weight or 0)
                product.amazon_price = item.price
                product.ean = item.ean
                product.jp_price = 0

                if weight:
                    product.oversea_shipping_cost = weight * settings.SHIPPING_BY_AIR_COST * settings.EXCHANCE_RATE
                    product.japan_shipping_cost = weight * settings.SHIPPING_BY_AIR_COST * settings.EXCHANCE_RATE

                    # product.japan_shipping_cost = max(product.japan_shipping_cost, 100000)
                else:
                    product.oversea_shipping_cost = 0
                    product.japan_shipping_cost = 0
                    product.on_sale = False

                if product.amazon_price:
                    for r in price_table:
                        if r[0] <= product.amazon_price <= r[1]:
                            product.margin_rate = r[2]

                    vn_price = int(product.amazon_price * settings.EXCHANCE_RATE)
                    product.margin_amount = int(vn_price * product.margin_rate/100)
                    product.sale_price = roundup(
                        vn_price + \
                        product.margin_amount + \
                        product.japan_shipping_cost + \
                        product.oversea_shipping_cost)
                else:
                    product.on_sale = False
                product.save()

        self.stdout.write(self.style.NOTICE('[stf_news] Update price ...'))
