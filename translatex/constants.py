# -*- coding: utf-8 -*-

TRANSLATOR_STATUS_OPTIONS = (
    (0, u'Chưa dịch', ),
    (1, u'Đang dịch', ),
    (2, u'Đã dịch', ),
)

TICKET_STATUS_OPTIONS = (
    (-1, u'Bỏ qua', ),
    (0, u'Chưa dịch', ),
    (1, u'Đang dịch', ),
    (2, u'Đã dịch', ),
    (3, u'Đang review', ),
    (4, u'Hoàn tất', ),
    (10, u'Rejected', ),
)

TICKET_REVIEW_OPTIONS = (
    (0, '-', ),
    (1, 'Bad', ),
    (2, 'So so', ),
    (3, 'OK', ),
    (4, 'Good', ),
    (5, 'Very Good', ),
)


def get_status_display(key):
    for c in TICKET_STATUS_OPTIONS:
        if c[0] == key:
            return c[1]
    return ''
