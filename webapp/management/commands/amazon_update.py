from django.core.management.base import BaseCommand, CommandError
from webapp.models import *
import urllib2, urllib
from bs4 import BeautifulSoup
from django.core.files import File
import ntpath
from re import sub
import re


class Command(BaseCommand):
    help = 'Correct DB after run migration'

    def handle(self, *args, **options):

        for item in Product.objects.exclude(asin='').filter(asin__isnull=False):
            print 'updating  %s' % item.vn_name
            amz_product = AmazonProduct.objects.filter(asin=item.asin).first()
            if not amz_product:
                AmazonProduct.objects.create(
                    asin=item.asin,
                    product=item
                )
            else:
                item.ean = amz_product.ean
                amz_product.product = item
                amz_product.save()
                item.save()

        self.stdout.write(self.style.SUCCESS('Done!'))
