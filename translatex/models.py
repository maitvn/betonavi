# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.core.files import File
import urllib
import json
from django.conf import settings
from webapp.models import *
from django.contrib.auth.models import User
from .constants import *
from datetime import datetime
import uuid
from django.db.models import Sum, Avg
from django.template.defaultfilters import truncatechars  # or truncatewords


class Translator(models.Model):
    user = models.ForeignKey(User, blank=True, null=True)
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    fullname = models.CharField(max_length=64, blank=True, null=True)

    bank_account_info = models.TextField(blank=True, null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def translated_num_in_month(self):
        now = datetime.now()

        return ProductTranslation.objects.filter(
            assignee=self,
            status=4,
            # closed_at__year=now.year,
            # closed_at__month=now.month,
            is_paid=False
        ).count()

    def rating_avg_in_month(self):
        now = datetime.now()
        return ProductTranslation.objects.filter(
            assignee=self,
            status=4,
            # closed_at__year=now.year,
            # closed_at__month=now.month,
            is_paid=False
        ).aggregate(Avg('rating'))['rating__avg']

    def charge_amount_in_month(self):
        now = datetime.now()
        return ProductTranslation.objects.filter(
            assignee=self,
            status=4,
            # closed_at__year=now.year,
            # closed_at__month=now.month,
            is_paid=False
        ).aggregate(Sum('charge_amount'))['charge_amount__sum']

    def __unicode__(self):
        return u'%s' % self.fullname


class ProductTranslation(models.Model):
    amazon_product = models.ForeignKey(AmazonProduct, blank=True, null=True)
    product = models.ForeignKey(Product, blank=True, null=True, on_delete=models.SET_NULL)
    assignee = models.ForeignKey(Translator, blank=True, null=True, verbose_name='Người dịch', on_delete=models.SET_NULL)
    reviewer1 = models.ForeignKey(User, blank=True, null=True, related_name='reviewer1', on_delete=models.SET_NULL)
    reviewer2 = models.ForeignKey(User, blank=True, null=True, related_name='reviewer2', on_delete=models.SET_NULL)

    # japanese fields
    jp_name = models.CharField(max_length=1024, blank=True, null=True)
    jp_introduction = models.TextField(blank=True, null=True)
    jp_description = models.TextField(blank=True, null=True)
    jp_content = models.TextField(null=True, blank=True)

    # vietnamese fields
    vn_name = models.CharField(max_length=1024, blank=True, null=True, verbose_name='Tên sản phẩm')
    vn_introduction = models.TextField(blank=True, null=True, verbose_name='Giới thiệu')
    vn_description = models.TextField(blank=True, null=True, verbose_name='Mô tả')
    vn_usage = models.TextField(null=True, blank=True, verbose_name='Cách sử dụng')
    vn_notice = models.TextField(null=True, blank=True, verbose_name='Chú ý')
    vn_content = models.TextField(null=True, blank=True)

    # reference url
    reference_url1 = models.CharField(max_length=512, null=True, blank=True, verbose_name='Link tham khảo 1')
    reference_url2 = models.CharField(max_length=512, null=True, blank=True, verbose_name='Link tham khảo 2')
    reference_url3 = models.CharField(max_length=512, null=True, blank=True, verbose_name='Link tham khảo 3')
    translated_time = models.IntegerField(default=0, null=True, blank=True, verbose_name='Thời gian dịch')
    translated_rate = models.IntegerField(default=0, null=True, blank=True, verbose_name='phần trăm dịch')

    similar_eans = models.TextField(null=True, blank=True)

    # Translated words count
    word_count = models.IntegerField(default=0, null=True, blank=True)
    charge_amount = models.IntegerField(default=0, null=True, blank=True)

    old_vn_name = models.CharField(max_length=1024, blank=True, null=True, verbose_name='Tên sản phẩm')
    old_word_count = models.IntegerField(default=0, null=True, blank=True)

    is_paid = models.BooleanField(default=False)

    is_done = models.BooleanField(default=False) # copy back to product master

    status = models.IntegerField(
        default=0,
        choices=TICKET_STATUS_OPTIONS,
        null=True,
        blank=True, verbose_name='Trạng thái')

    rating = models.IntegerField(
        default=0,
        choices=TICKET_REVIEW_OPTIONS,
        null=True,
        blank=True, verbose_name='Đánh giá')

    closed_at = models.DateTimeField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @property
    def similar_product_count(self):
        return len(self.similar_eans.split(',')) if self.similar_eans else 0

    @property
    def similar_products(self):
        if self.similar_eans:
            return ProductTranslation.objects.filter(
                amazon_product__ean__in=self.similar_eans.split(','))
        return None

    @property
    def short_title(self):
        return truncatechars(self.amazon_product.title, 100) if self.amazon_product else ''

    @property
    def short_vn_name(self):
        return truncatechars(self.vn_name, 100)

    @property
    def estimate(self):
        charge_amount = 0

        translatedRate = self.translated_rate or 0

        if self.status > 1:
            # base amount
            charge_amount = 6000

            # translation rate
            charge_amount += max(translatedRate - 50, 0) * 100

            # word count (length of content)
            if self.word_count < 200:
                charge_amount += -2000
            elif 400 < self.word_count < 700:
                charge_amount += 2000
            elif self.word_count >= 700:
                charge_amount += 5000

            # rating
            charge_amount += 3000

        return charge_amount

    def save(self, *args, **kwargs):
        # couting words
        name_len = len(self.vn_name.split()) if self.vn_name else 0
        introduction_len = len(self.vn_introduction.split()) if self.vn_introduction else 0
        description_len = len(self.vn_description.split()) if self.vn_description else 0
        usage_len = len(self.vn_usage.split()) if self.vn_usage else 0
        notice_len = len(self.vn_notice.split()) if self.vn_notice else 0
        self.word_count = name_len + introduction_len + description_len + usage_len + notice_len
        super(ProductTranslation, self).save(*args, **kwargs)


class TranslationComment(models.Model):
    product = models.ForeignKey(ProductTranslation, blank=True, null=True)
    user = models.ForeignKey(User, blank=True, null=True)
    text = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Billing(models.Model):
    translator = models.ForeignKey(Translator, blank=True, null=True)
    year = models.IntegerField(choices=((2017, 2017,), (2018, 2018, ), ),blank=True, null=True)
    month = models.IntegerField(blank=True, null=True)
    charge_amount = models.IntegerField(blank=True, null=True)
    bonus = models.IntegerField(default=0, blank=True, null=True)
    is_paid = models.BooleanField(default=False)
    paid_at = models.DateTimeField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Notification(models.Model):
    product = models.ForeignKey(ProductTranslation, blank=True, null=True)
    user = models.ForeignKey(User, blank=True, null=True)
    message = models.CharField(max_length=200, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
