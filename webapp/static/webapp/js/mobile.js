// using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var LtNotify = LTNotify = (function () {
  var timeout = null;
  if(!document.getElementById('lt-notify')){
    document.body.innerHTML += '<div id="lt-notify" style="display:none;"></div>';
  }
  var rootEl = document.getElementById('lt-notify');
  rootEl.addEventListener('click', function () {
    rootEl.style.display = 'none';
  });
  rootEl.addEventListener('mouseover', function () {
    clearTimeout(timeout);
  });
  rootEl.addEventListener('mouseout', function () {
    timeout = setTimeout(function () {
      rootEl.style.display = 'none';
    }, 3000);
  });
  return {
    notify: function (msg) {
      clearTimeout(timeout);
      rootEl.innerHTML = msg;
      rootEl.className = "";
      rootEl.style.display = 'inline-block';
      timeout = setTimeout(function () {
        rootEl.style.display = 'none';
      }, 3000);
    },
    error: function (msg) {
      clearTimeout(timeout);
      rootEl.innerHTML = msg;
      rootEl.style.display = 'inline-block';
      rootEl.className = "error";
      timeout = setTimeout(function () {
        rootEl.style.display = 'none';
      }, 4000);
    }
  }
})();
function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}



$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
        }
    }
});


var btnv = btnv || {};
btnv.http = {
  send: function (method, url, data, successCb, errorCb) {
    $.ajax({
      url: url,
      method: method,
      contentType: "application/json",
      data: JSON.stringify(data),
      success: function (res) {
        successCb && successCb(res)
      },
      error: function (res) {
        // console.log(res)
        if (res.status === 400 || res.status === 403) {
          LtNotify.error(
            'Bạn cần là thành viên để sử dụng chức năng này!'
          )
        }
        errorCb && errorCb(res)
      }
    });
  },
  addCart: function (data, callback) {
    this.send('POST', '/api/cart', data, function (res) {
      // success
      LtNotify.notify('Sản phẩm đã được thêm vào giỏ hàng!')
      callback && callback(res);
    }, function () {
      // error
      // LtNotify.error('Error')
    })
  },
  updateCart: function (data, callback) {
    this.send('PUT', '/api/cart', data, function (res) {
      // success
      // LtNotify.notify('Success')
      callback && callback(res);
    }, function () {
      // error
      // LtNotify.error('Error')
    })
  },
  removeCart: function (data, callback) {
    this.send('DELETE', '/api/cart', data, function (res) {
      // success
      // LtNotify.notify('Success')
      callback && callback(res);
    }, function () {
      // error
      // LtNotify.error('Error')
    })
  },
  addFavorite: function (data, callback) {
    this.send('POST', '/api/favorite', data, function (res) {
      // success
      LtNotify.notify('Sản phẩm đã được lưu!')
      callback && callback(res);
    }, function () {
      // error
      // LtNotify.error('Error')
    })
  },
  removeFavorite: function (data, callback) {
    this.send('DELETE', '/api/favorite', data, function (res) {
      // success
      // LtNotify.notify('Xoá thành công!')
      callback && callback(res);
    }, function () {
      // error
      // LtNotify.error('Error')
    })
  },
  followEmail: function (data, callback) {
    this.send('POST', '/api/follow_email', data, function (res) {
      // success
      // LtNotify.notify('Xoá thành công!')
      callback && callback(res);
    }, function () {
      // error
      // LtNotify.error('Error')
    })
  },
  requestProduct: function (data, callback) {
    this.send('POST', '/api/request_product', data, function (res) {
      // success
      // LtNotify.notify('Xoá thành công!')
      callback && callback(res);
    }, function () {
      // error
      // LtNotify.error('Error')
    })
  },
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function convertToInt(x) {
    return parseInt(x.replace(",", ""));
}

function calculateCart() {

  if ($('.out_of_stock').length === 0) {

    if ($('#total-combined-price').length === 0) {
      return true;
    }
    if ($('.cart-list .item:visible').length === 0) {
      $('#cart-summary').remove();
    }

    var total = 0;
    $('.cart-list .item:visible').each(function () {
      const quantity = parseInt($(this).find('.cart-quantity').val())
      const unitPrice = convertToInt($(this).find('.unit-price').attr('unitprice'))
      total += quantity * unitPrice
    })

    $('#total-combined-price').text(numberWithCommas(total));

    $('.notify-out-off-stock').addClass('hidden')

    $('.buy-btn').removeClass('hidden')

  } else {
    // out of stock
    $('.notify-out-off-stock').removeClass('hidden')

    $('.buy-btn').addClass('hidden')
  }
}

$(document).ready(function () {
    // calculate price when rendering
    calculateCart();

    $('.img-slider img').click(function (e) {
      // $('#large-image').attr('src', $(this).attr('src'))
      $('#large-image').css('background-image', 'url(' + $(this).attr('src') + ')')
    })

    // Toggle side menu
    $('#menu-btn').click(function () {
      console.log('open menu')
      $('#menu-panel').show();
    })

    $('#menu-panel').click(function(e) {
      if (e.target === $(this).get(0)) {
        $(this).hide()
      }
    })


    // Events binding
    $('.btn.add-to-cart').click(function () {

        $('.option .error').hide();
        // validate color
        if ($('.option .color').length > 0 && $('.option .color.selected').length === 0){
          $('.option .err-color').show()
          return false;
        }

        // validate size
        if ($('.option .size').length > 0 && $('.option .size.selected').length === 0){
          $('.option .err-size').show();
          return false;
        }

        color = '';
        if ($('.option .color.selected')) {
          color = $('#color-name').text().trim();
        }
        size = '';
        if ($('.option .size.selected')) {
          size = $('.option .size.selected').text().trim();
        }
        var data = {
          product_id: parseInt($(this).attr('pid')),
          flag: true,
          color: color,
          size: size
        }

        btnv.http.addCart(data, function (res) {
          $('#cart-quantity').text(res.count);
          console.log(res)
        });
    })

    $('.btn.save').click(function () {
        btnv.http.addFavorite({product_id: parseInt($(this).attr('pid')), flag: true}, function (res) {
          $('#saved-quantity').text(res.count);
        });
    })

    $('.btn.remove-favorite').click(function (res) {
        var $target = $(this).closest('.item');
        btnv.http.removeFavorite({saved_id: parseInt($(this).attr('sid'))}, function (res) {
          $('#saved-quantity').text(res.count);

          $target.hide('slow');
        });
    })

    $('.btn.remove-cart').click(function (res) {
        var $target = $(this).closest('.item');
        btnv.http.removeCart({cart_id: parseInt($(this).attr('cid'))}, function (res) {
          $('#cart-quantity').text(res.count);

          $target.hide('slow', function () {
            calculateCart();
          });
        });
    })

    $('.select.cart-quantity').change(function (res) {
      btnv.http.updateCart({
        cart_id: parseInt($(this).attr('cid')),
        quantity: $(this).val()
      }, function (res) {
        $('#cart-quantity').text(res.count);
      });
      calculateCart();
    })

    $('#search-btn').click(function () {
        keyword = $('#search-keyword').val().trim()
        window.location.href = '/search?q=' + keyword;
    })

    $('#search-keyword').keyup(function (event) {
        if (event.keyCode == 13 ) {
             keyword = $('#search-keyword').val().trim()
            window.location.href = '/search?q=' + keyword;
        }
    })

    $('#campainPopup .overlay').click(function (event) {
      // console.log(event.target)
      $('#campainPopup').hide();
    })

    $('#sendCampainEmail').click(function () {
      var email = $('#campainEmail').val().trim()

      if (validateEmail(email)) {
        btnv.http.followEmail({
          'email': email
        }, function () {
          $('#campainPopup').hide();
        })
      } else {
        $('#campainEmail').focus();
      }
    })


    setInterval(function () {
      $('#requestProductBtn').addClass('animated rotateIn');
      // $('#requestProductBtn').addClass('animated bounceOutLeft');
      setTimeout(function () {
        $('#requestProductBtn').removeClass('animated rotateIn');
      }, 1000)
    }, 10000)

    $('#requestProductBtn').click(function () {
      $('#requestProduct').show();
    })

    $('#requestProduct .overlay').click(function (event) {
      // console.log(event.target)
      $('#requestProduct').hide();
    })

    $('#sendRequestProduct').click(function () {
      var email = '';
      if ($('#requestProductEmail').length > 0) {
        email = $('#requestProductEmail').val().trim()
        if (!validateEmail(email)) {
          LtNotify.error(
            'Địa chỉ Email của bạn cần nhập lại!'
          )
          $('#requestProductEmail').focus()
          return false;
        }
      }

      var content = $('#requestProductContent').val().trim()
      if (content.length < 20) {
        LtNotify.error(
          'Thông tin sản phẩm quá ít, xin vui lòng thêm thông tin!'
        )
        $('#requestProductContent').focus()
        return false;
      }

      btnv.http.requestProduct({
        'email': email,
        'content': content
      }, function () {
        $('#requestProductContent').val('')
        $('#requestProduct').hide();
        LtNotify.notify(
          'Cảm ơn đóng góp của bạn!'
        )
      })
    })

    $('.option .color div').hover(function () {
      $('#color-name').text($(this).attr('data-name'));
    }, function () {
      $('#color-name').text($('#color-name').attr('data-persist'));
    })

    $('.option .color div').click(function () {
      $('.color').removeClass('selected');
      $(this).parent('.color').addClass('selected');
      $('#color-name').text($(this).attr('data-name'));
      $('#color-name').attr('data-persist', $(this).attr('data-name'));
    })

    $('.option .size').click(function () {
      $('.option .size').removeClass('selected');
      $(this).addClass('selected');
    })


    $('#messenger-plugin .messenger-button').click(function () {
        $('#messenger-plugin .fb-page, #messenger-plugin .close').show();
        $(this).hide();
    })

    $('#messenger-plugin .close').click(function () {
        $('#messenger-plugin .fb-page, #messenger-plugin .close').hide();
        $('#messenger-plugin .messenger-button').show();
    })



})

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
