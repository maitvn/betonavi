# -*- coding: utf-8 -*-
from .models import *
from django.http import HttpResponse


def notification(view_func):
    """
        Get notfications

    """
    def func_wrapper(request, *args, **kwargs):

        if request.user.is_staff:
            request.notifications = Notification.objects.all().select_related('product').order_by('-id')[:30]
        else:
            request.notifications = Notification.objects.filter(
                product__assignee__user=request.user
            ).order_by('-id')

        return view_func(request, *args, **kwargs)

    return func_wrapper


def permission(view_func):
    """
        Check Permissions
    """
    def func_wrapper(request, *args, **kwargs):

        if not request.user.is_staff:
            if not Translator.objects.filter(user=request.user).exists():
                return HttpResponse("(403) Permission Denied. Bạn không có quyền truy cập trang này!")

        return view_func(request, *args, **kwargs)

    return func_wrapper
