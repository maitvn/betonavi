from django.utils.deprecation import MiddlewareMixin
from webapp.models import Cart, Favorite, Category
from django.db.models import Sum

class ECMiddleware(MiddlewareMixin):
    def process_request(self, request):
        categories = []
        for cate in Category.objects.filter(parent__isnull=True):
            categories.append({
                'id': cate.id,
                'slug': cate.slug,
                'name': cate.name,
                'color': cate.color
            })
        request.session['categories'] = categories

        request.session['cart_number'] = 0
        if request.user and not request.user.is_anonymous():
            request.session['cart_number'] = Cart.objects.filter(user=request.user).aggregate(Sum('quantity'))['quantity__sum'] or 0

        request.session['saved_number'] = 0
        if request.user and not request.user.is_anonymous():
            request.session['saved_number'] = Favorite.objects.filter(user=request.user).count()

        return None
