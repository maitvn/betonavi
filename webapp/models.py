# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
from django.core.files import File
import urllib
# from tinymce.models import HTMLField
from django.template.defaultfilters import slugify
import json
from django.conf import settings
import uuid
from django.db.models import Q


class Profile(models.Model):
    user = models.OneToOneField(User, primary_key=True)
    picture = models.ImageField(upload_to='users/', blank=True, null=True)
    bio = models.TextField(blank=True)
    url = models.CharField(max_length=512, blank=True)
    image_url = models.URLField(blank=True)

    def get_remote_image(self):
        if self.image_url and not self.picture:
            result = urllib.urlretrieve(self.image_url)
            self.picture.save(
                    '%s%d.jpg' % ('user_', self.user_id),
                    File(open(result[0]))
                    )
            self.save()


class OverseaShippingFee(models.Model):
    type = models.CharField(max_length=128, null=True, blank=True)
    price= models.IntegerField(null=True, blank=True)
    ovt_price= models.IntegerField(null=True, blank=True)
    jk_price= models.IntegerField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return u'%s' % self.type


class Category(models.Model):
    name = models.CharField(max_length=128)
    slug = models.SlugField(max_length=512, null=True, blank=True)
    description = models.CharField(max_length=128, blank=True)
    parent = models.ForeignKey("self", on_delete=models.CASCADE, null=True, blank=True, related_name='subs',)
    image = models.ImageField(upload_to='category', blank=True, null=True)
    small_image = models.ImageField(upload_to='category', blank=True, null=True)
    display_order = models.SmallIntegerField(default=999, null=True, blank=True)
    color = models.CharField(max_length=7, default='#f28d23', null=True)
    status = models.SmallIntegerField(default=0, choices=(
        (0, 'Show',),
        (100, 'Hide',),
    ))

    @property
    def features(self):
        return Product.objects.filter(
        Q(category_id=self.id) | Q(
            Q(category__parent__isnull=False) &
            Q(category__parent__id=self.id)
        ) | Q(
            Q(category__parent__isnull=False) &
            Q(category__parent__parent__isnull=False) &
            Q(category__parent__parent__id=self.id)
        ),
        sale_price__isnull=False).order_by('-score', '-id')[:10]

    @property
    def features_mobile(self):
        return Product.objects.filter(
            Q(category_id=self.id) | Q(
                Q(category__parent__isnull=False) &
                Q(category__parent__id=self.id)
            ) | Q(
                Q(category__parent__isnull=False) &
                Q(category__parent__parent__isnull=False) &
                Q(category__parent__parent__id=self.id)
            ),
            sale_price__isnull=False).order_by('-score', '-id')[:9]

    @property
    def mail_magazine_products(self):
        return Product.objects.filter(
            Q(category_id=self.id) | Q(
                Q(category__parent__isnull=False) &
                Q(category__parent__id=self.id)
            ) | Q(
                Q(category__parent__isnull=False) &
                Q(category__parent__parent__isnull=False) &
                Q(category__parent__parent__id=self.id)
            ),
            is_mail_pr=True,
            sale_price__isnull=False).order_by('-score', '-id')

    def save(self, *args, **kwargs):
        if not self.slug and self.name:
            self.slug = slugify(self.name)
        super(Category, self).save(*args, **kwargs)

    class Meta:
        ordering = ['-display_order']

    def __unicode__(self):
        name = u'%s' % self.name
        if self.parent:
            name = '%s / %s' % (self.parent.name, name)

        if self.parent and self.parent.parent:
            name = '%s / %s' % (self.parent.parent.name, name)

        return name


class Provider(models.Model):
    name = models.CharField(max_length=128)
    description = models.CharField(max_length=128, blank=True)
    matching_urls = models.TextField(null=True)
    image = models.ImageField(upload_to='provider/', blank=True, null=True)

    def __unicode__(self):
        return u'%s' % self.name


class Product(models.Model):
    category = models.ForeignKey(Category, blank=True, null=True)
    sub_category = models.ForeignKey(Category, blank=True, null=True, related_name='sub')
    overseashippingfee = models.ForeignKey(OverseaShippingFee, blank=True, null=True)
    asin = models.CharField(max_length=64, null=True, blank=True)
    ean = models.CharField(max_length=20, blank=True, null=True)
    slug = models.SlugField(max_length=512, null=True, blank=True)
    vn_name = models.CharField(max_length=1024, blank=True, null=True)
    vn_introduction = models.TextField(blank=True, null=True)
    vn_description = models.TextField(blank=True, null=True)
    vn_content = models.TextField(null=True, blank=True)
    jp_detail_table = models.TextField(blank=True, null=True)
    vn_detail_table = models.TextField(blank=True, null=True)
    image = models.ImageField(upload_to='product/', blank=True, null=True)
    weight = models.IntegerField(null=True, blank=True)
    shipping_weight = models.IntegerField(null=True, blank=True)

    # variaty
    color_json = models.TextField(blank=True, null=True)
    size_json = models.TextField(blank=True, null=True)

    # prices
    sale_price = models.IntegerField(null=True, blank=True)
    price_off = models.IntegerField(default=0, null=True, blank=True)
    jp_price = models.IntegerField(null=True, blank=True)
    amazon_price = models.IntegerField(null=True, blank=True)
    jp_price_adjusted = models.IntegerField(null=True, blank=True)

    # shipping cost
    japan_shipping_cost = models.IntegerField(default=0, null=True, blank=True)
    oversea_shipping_cost = models.IntegerField(default=0, null=True, blank=True)

    # stock & availability
    availability = models.BooleanField(default=True)
    on_sale = models.BooleanField(default=True)

    # margin
    margin_rate = models.IntegerField(default=5, null=True, blank=True)
    margin_amount = models.IntegerField(null=True, blank=True)


    # user generated
    score = models.IntegerField(default=0, null=True, blank=True)
    rank = models.IntegerField(default=0, null=True, blank=True)
    view_count = models.IntegerField(default=0, null=True, blank=True)
    sources = models.ManyToManyField(Provider, through='Source', related_name='sources')

    added_by = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    status = models.SmallIntegerField(default=0)
    crawl_status = models.SmallIntegerField(default=0)

    def save(self, *args, **kwargs):
        if not self.slug and self.vn_name:
            self.slug = slugify(self.vn_name)
        super(Product, self).save(*args, **kwargs)

    def __unicode__(self):
        return u'%s' % self.vn_name

    class Meta:
        ordering = ['-id']


class EditProduct(Product):
    class Meta:
        proxy=True


class MailMagazineProduct(Product):
    class Meta:
        proxy=True

class EditCategoryProduct(Product):
    class Meta:
        proxy=True


class AmazonProduct(models.Model):
    # Get from Amazon Api
    title = models.CharField(max_length=1024, blank=True, null=True)
    ean = models.CharField(max_length=20, blank=True, null=True)
    asin = models.CharField(max_length=20, blank=True, null=True)
    brand = models.CharField(max_length=128, blank=True, null=True)
    product_group = models.CharField(max_length=128, blank=True, null=True)
    product_type_name = models.CharField(max_length=128, blank=True, null=True)
    reviews_url = models.CharField(max_length=1024, blank=True, null=True)
    sales_rank = models.IntegerField(blank=True, null=True)
    images_list = models.TextField(blank=True, null=True)
    large_image_url = models.TextField(blank=True, null=True)
    medium_image_url = models.TextField(blank=True, null=True)
    price = models.IntegerField(blank=True, null=True)
    availability = models.BooleanField(default=True)

    scanned_count = models.IntegerField(default=0)
    liked_count = models.IntegerField(default=0)

    manufacturer = models.CharField(max_length=200, blank=True, null=True)
    publisher = models.CharField(max_length=200, blank=True, null=True)
    features = models.TextField(blank=True, null=True)
    detail = models.TextField(blank=True, null=True)


    # Price management

    price_history = models.TextField(blank=True, null=True)

    # Get from chrome extension
    weight = models.CharField(max_length=128, null=True, blank=True)
    shipping_weight = models.CharField(max_length=128, null=True, blank=True)
    rating = models.CharField(max_length=512, null=True, blank=True)
    rank = models.CharField(max_length=512, null=True, blank=True)

    # management fields
    product = models.ForeignKey(Product, null=True, blank=True, on_delete=models.SET_NULL)
    is_scanned = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    amazon_fetched_at = models.DateTimeField(null=True, blank=True)
    status = models.SmallIntegerField(default=0)

    def __unicode__(self):
        return u'%s' % self.title


class Banner(models.Model):
    name = models.CharField(max_length=128)
    caption = models.CharField(max_length=128, null=True)
    link = models.CharField(max_length=512, null=True)
    image = models.ImageField(upload_to='banner/', blank=True, null=True)
    display_order = models.SmallIntegerField(default=999, null=True, blank=True)

    class Meta:
        ordering = ['display_order']

    def __unicode__(self):
        return u'%s' % self.name


class Favorite(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)


class Cart(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.SmallIntegerField(default=1, null=True)
    color = models.CharField(max_length=128, null=True, blank=True)
    size = models.CharField(max_length=128, null=True, blank=True)


class Source(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    Provider = models.ForeignKey(Provider, on_delete=models.CASCADE)
    is_second_hand = models.BooleanField(default=False)
    link = models.CharField(max_length=512, null=True, blank=True)
    source_product_code = models.CharField(max_length=64, null=True)
    ja_price = models.IntegerField(null=True, blank=True)
    ja_price_min = models.IntegerField(null=True, blank=True)
    ja_price_max = models.IntegerField(null=True, blank=True)


class ProductImage(models.Model):
    image_height = models.IntegerField(null=True, default=0)
    image_width = models.IntegerField(null=True, default=0)
    image = models.ImageField(
        upload_to='images/',
        height_field='image_height',
        width_field='image_width',
        verbose_name='URL')
    product = models.ForeignKey(
        Product,
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        related_name="images"
    )


class ShippingAddress(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    shipping_fullname = models.CharField(max_length=128, null=True, blank=True)
    shipping_phone = models.CharField(max_length=16, null=True, blank=True)
    shipping_address1 = models.CharField(max_length=512, null=True, blank=True)
    shipping_address2 = models.CharField(max_length=512, null=True, blank=True)
    shipping_prefecture = models.CharField(max_length=512, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @property
    def shipping_address(self):
        return '%s, %s, %s' % (
        self.shipping_address1,
        self.shipping_address2,
        self.shipping_prefecture)

    class Meta:
        ordering = ['-updated_at']


class Order(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    order_number = models.CharField(max_length=16, null=True, blank=True)

    vn_total_price = models.IntegerField(null=True, blank=True)
    jp_total_price = models.IntegerField(null=True, blank=True)
    shipping_fee = models.IntegerField(null=True, blank=True)
    total_weight = models.IntegerField(default=0, null=True, blank=True)

    vn_order_amount = models.IntegerField(null=True, blank=True)
    vn_paid_amount = models.IntegerField(null=True, blank=True)

    # saleoff price - xxx
    price_off = models.IntegerField(default=0, null=True, blank=True)


    shipping_method = models.CharField(max_length=128, null=True, blank=True)
    shipping_fullname = models.CharField(max_length=128, null=True, blank=True)
    shipping_phone = models.CharField(max_length=16, null=True, blank=True)
    shipping_prefecture = models.CharField(max_length=512, null=True, blank=True)
    shipping_address1 = models.CharField(max_length=512, null=True, blank=True)
    shipping_address2 = models.CharField(max_length=512, null=True, blank=True)

    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='buyer')

    staff = models.ForeignKey(User, on_delete=models.CASCADE, null=True, related_name='staff')
    ordred_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    paid_at = models.DateTimeField(null=True, blank=True)
    completed_at = models.DateTimeField(null=True, blank=True)

    status = models.IntegerField(
        default=settings.ORDER_STATUS_ORDERED,
        choices=settings.ORDER_STATUS_CHOICES,
        null=True,
        blank=True)

    sent_order_confirm_mail = models.BooleanField(default=False)
    sent_order_delivering_mail = models.BooleanField(default=False)
    sent_order_complete_mail = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @property
    def pay_amount(self):
        return self.vn_total_price - self.price_off

    class Meta:
        ordering = ['-id']

    def __unicode__(self):
        return u'%s' % self.order_number

    @property
    def shipping_address(self):
        return '%s, %s, %s' % (
        self.shipping_address1,
        self.shipping_address2,
        self.shipping_prefecture)

    @property
    def is_paid(self):
        return self.status >= settings.ORDER_STATUS_PAID

    @property
    def is_delivering(self):
        return self.status >= settings.ORDER_STATUS_DELIVERING

    @property
    def is_completed(self):
        return self.status == settings.ORDER_STATUS_COMPLETED


class OrderItem(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name='items')
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField(default=1, null=True, blank=True)
    color = models.CharField(max_length=128, null=True, blank=True)
    size = models.CharField(max_length=128, null=True, blank=True)
    vn_item_price = models.IntegerField(null=True, blank=True)
    jp_item_price = models.IntegerField(null=True, blank=True)
    vn_total_price = models.IntegerField(null=True, blank=True)
    jp_total_price = models.IntegerField(null=True, blank=True)


class OrderMessage(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name='message')
    message = models.CharField(max_length=512, null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='send_by_buyer')
    staff = models.ForeignKey(User, on_delete=models.CASCADE, null=True, related_name='send_by_staff')
    is_staff_reply = models.BooleanField(default=False)
    is_mail_user = models.BooleanField(default=True)
    status = models.IntegerField(default=0, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-id']


class RequestInvitation(models.Model):
    email = models.CharField(max_length=128, null=True, blank=True)
    message = models.TextField(null=True, blank=True)
    is_notify_admin = models.BooleanField(default=False)
    is_send_invite = models.BooleanField(default=False)

    reply_message = models.TextField(null=True, blank=True)
    is_ready_send_reply = models.BooleanField(default=False)
    is_reply = models.BooleanField(default=False)

    status = models.IntegerField(default=0, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Option(models.Model):
    key = models.CharField(max_length=64, choices=settings.OPTION_CHOICES, null=True, blank=True)
    text_value = models.CharField(max_length=64, null=True, blank=True)
    number_value = models.IntegerField(default=0, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class PriceTable(models.Model):
    min_price = models.IntegerField(default=0, null=True, blank=True)
    max_price = models.IntegerField(default=0, null=True, blank=True)
    rate = models.IntegerField(default=10, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class BankAccount(models.Model):
    bank_name = models.CharField(max_length=256, null=True, blank=True)
    branch_name = models.CharField(max_length=256, null=True, blank=True)
    owner_name = models.CharField(max_length=64, null=True, blank=True)
    number = models.CharField(max_length=64, null=True, blank=True)
    note = models.CharField(max_length=512, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class FeatureSet(models.Model):
    name = models.CharField(max_length=256, null=True, blank=True)
    slug = models.SlugField(max_length=512, null=True, blank=True)
    category = models.ForeignKey(Category, blank=True, null=True)
    image = models.ImageField(upload_to='set', blank=True, null=True)
    description = models.TextField(null=True, blank=True)
    content = models.TextField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        if not self.slug and self.name:
            self.slug = slugify(self.name)
        super(FeatureSet, self).save(*args, **kwargs)


class SetItem(models.Model):
    feature_set = models.ForeignKey(FeatureSet, on_delete=models.CASCADE, related_name='items')
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField(default=1, null=True, blank=True)
    description = models.TextField(null=True, blank=True)


class Coupon(models.Model):
    name = models.CharField(max_length=256, null=True, blank=True)
    image = models.ImageField(upload_to='coupon', blank=True, null=True)
    description = models.TextField(null=True, blank=True)
    code = models.CharField(max_length=8, null=True, blank=True)
    price_off = models.IntegerField(default=0, null=True, blank=True)
    is_invitation_coupon = models.BooleanField(default=False)
    status = models.IntegerField(default=0, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class ProductRequest(models.Model):
    user = models.ForeignKey(User, blank=True, null=True)
    email = models.CharField(max_length=128, null=True, blank=True)
    content = models.TextField(null=True, blank=True)
    is_notify_admins = models.BooleanField(default=False)
    status = models.IntegerField(default=0, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class PickupPoint(models.Model):
    contact_fullname = models.CharField(max_length=128, null=True, blank=True)
    contact_phone = models.CharField(max_length=16, null=True, blank=True)
    address1 = models.CharField(max_length=512, null=True, blank=True)
    address2 = models.CharField(max_length=512, null=True, blank=True)
    prefecture = models.CharField(max_length=512, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @property
    def shipping_address(self):
        return '%s, %s, %s' % (
        self.address1,
        self.address2,
        self.prefecture)

    class Meta:
        ordering = ['-updated_at']


class MailMagazine(models.Model):
    subject = models.CharField(max_length=512)
    categories = models.ManyToManyField(Category)
    is_processed = models.BooleanField(default=False)
    scheduled_at = models.DateTimeField()

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class SearchKeyword(models.Model):
    keyword = models.CharField(max_length=512)
    is_from_mobile = models.BooleanField(default=False)
    count = models.IntegerField(default=1)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return u'%s' % self.keyword
