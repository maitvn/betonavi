from rest_framework.response import Response
from serializers import RequestSerializer
from django.conf import settings
from django.db import transaction, IntegrityError
from django.core.exceptions import ObjectDoesNotExist
from distutils.version import StrictVersion

from .models import Device


def api_common(view_func):
    """
        Common process for mobile APIs

    """
    def func_wrapper(request, *args, **kwargs):
        serializer = RequestSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                'code': 100,
                'message': serializer.errors
            }, status=400)
        else:
            # store device.
            if serializer.data['client_key'] != settings.CLIENT_KEY:
                return Response({
                    'code': 110,
                    'message': 'Client Key Authentication Failed!'
                }, status=400)

            device, created = Device.objects.get_or_create(
                device_uuid=serializer.data['device_uuid'],
                defaults={
                    'device_manufacturer': serializer.data['device_manufacturer'],
                    'device_name': serializer.data['device_name'],
                    'device_model': serializer.data['device_model'],
                    'device_locale': serializer.data['device_locale'],
                    'device_country': serializer.data['device_country'],
                    'device_system_name': serializer.data['device_system_name'],
                    'device_id': serializer.data['device_id'],
                    'device_version': serializer.data['device_version'],
                    'bundle_id': serializer.data['bundle_id'],
                    'build_number': serializer.data['build_number'],
                    'app_version': serializer.data['app_version'],
                    'app_version_readable': serializer.data['app_version_readable'],
                },
            )

            request.device = device

            return view_func(request, *args, **kwargs)

    return func_wrapper
