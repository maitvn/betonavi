# -*- coding: utf-8 -*-
from rest_framework import serializers
from .models import *

class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = ('id', 'name')


class ProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = ('asin', 'vn_name', 'vn_introduction', 'vn_description', 'category_id')


class CartSerializer(serializers.Serializer):
    product_id = serializers.IntegerField(required=False)
    quantity = serializers.IntegerField(required=False)


class FavoriteSerializer(serializers.Serializer):
    saved_id = serializers.IntegerField(required=False)
    product_id = serializers.IntegerField(required=False)


class AmazonProductSerializer(serializers.Serializer):
    asin = serializers.CharField(max_length=16, required=True)
    category_id = serializers.IntegerField(required=False, default=None)
    jp_price = serializers.CharField()
    vn_price = serializers.CharField(required=False, allow_null=True)

    jp_name = serializers.CharField()
    vn_name = serializers.CharField(required=False, allow_null=True)

    jp_description = serializers.CharField(required=False, allow_null=True)
    vn_description = serializers.CharField(required=False, allow_null=True)

    jp_detail_table = serializers.CharField(required=False, allow_null=True)
    vn_detail_table = serializers.CharField(required=False, allow_null=True)

    images = serializers.CharField(required=True)
