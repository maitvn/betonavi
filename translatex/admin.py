# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.html import format_html
from django.contrib import admin

from .models import *
# Register your models here.

@admin.register(ProductTranslation)
class ProductTranslationAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'display_image',
        'short_title',
        'old_vn_name',
        'old_word_count',
        'ptp_link',
        'amazon_link',
        'btnv_link',
        'assignee',
        'status',
    )
    # list_filter = ('parent', )
    list_editable = ('status', 'assignee')

    search_fields = ['amazon_product__ean', 'amazon_product__asin', 'amazon_product__title']

    def display_image(self, obj):
        if obj.amazon_product:
            return format_html(u'<img src="{}" style="height:40px" alt="" />', obj.amazon_product.medium_image_url)
        return '-'
    display_image.short_description = 'Image'

    def ptp_link(self, obj):
        return format_html('<a target="_blank" href="https://www.betonavi.com/translatex/sp/{}">link</a>', obj.id)

    ptp_link.short_description = 'ptp_link'

    def btnv_link(self, obj):
        return format_html('<a target="_blank" href="https://www.betonavi.com/dp/{}">link</a>', obj.amazon_product.asin)

    btnv_link.short_description = 'btnv_link'

    def amazon_link(self, obj):
        return format_html('<a target="_blank" href="https://www.amazon.co.jp/dp/{}">link</a>', obj.amazon_product.asin)

    amazon_link.short_description = 'amazon_link'



@admin.register(Translator)
class TranslatorAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'fullname',
    )
    # list_filter = ('parent', )


@admin.register(Billing)
class BillingAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'translator',
        'year',
        'month',
        'charge_amount',
        'bonus',
        'is_paid',
    )
    # list_filter = ('parent', )
