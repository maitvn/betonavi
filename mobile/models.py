# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.core.files import File
import urllib
import json
from django.conf import settings
from webapp.models import AmazonProduct


class Device(models.Model):
    device_uuid = models.CharField(max_length=200, blank=True, null=True)

    device_manufacturer = models.CharField(max_length=200, blank=True, null=True)
    device_name = models.CharField(max_length=200, blank=True, null=True)
    device_model = models.CharField(max_length=200, blank=True, null=True)
    device_locale = models.CharField(max_length=200, blank=True, null=True)
    device_country = models.CharField(max_length=200, blank=True, null=True)
    user_agent = models.CharField(max_length=2000, blank=True, null=True)
    device_system_name = models.CharField(max_length=200, blank=True, null=True)
    device_id = models.CharField(max_length=200, blank=True, null=True)
    device_version = models.CharField(max_length=200, blank=True, null=True)
    bundle_id = models.CharField(max_length=200, blank=True, null=True)
    build_number = models.CharField(max_length=200, blank=True, null=True)
    app_version = models.CharField(max_length=200, blank=True, null=True)
    app_version_readable = models.CharField(max_length=200, blank=True, null=True)


    notification = models.BooleanField(default=True)
    fcm_token = models.CharField(max_length=512, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Favorite(models.Model):
    device = models.ForeignKey(Device, blank=True, null=True)
    amazon_product = models.ForeignKey(AmazonProduct, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
