
var data = {
  product: {
    asin: '',
    jp_name: '',
    vn_name: '',
    jp_price: '-',
    jp_description: '',
    vn_introduction: '',
    vn_description: '',
    jp_detail_table: {},
    images: [],
    category_id: 0,
  },
  categories: [],
  fetching: false,
  isAdded: false,
  isSaving: false
}

chrome.runtime.onMessage.addListener(function (msg, sender, response) {

    if ((msg.from === 'background') && (msg.subject === 'openPopup')) {
        data.product.asin = msg.data.asin;
        data.product.jp_name = msg.data.jp_name;
        data.product.jp_price = msg.data.jp_price;
        data.product.jp_description = msg.data.jp_description;
        data.product.jp_detail_table = msg.data.jp_detail_table;
        data.product.images = msg.data.images;

        chrome.runtime.sendMessage({
            from: 'iframe',
            subject: 'category',
        }, (res) => {
            data.categories = res.categories;
        })

        data.fetching = true
        chrome.runtime.sendMessage({
            from: 'iframe',
            subject: 'check',
            data: {asin: data.product.asin}
        }, (res) => {
            data.fetching = false
            if (res.product.asin) {
              data.isAdded = true
              data.product.vn_name = res.product.vn_name
              data.product.category_id = res.product.category_id
              data.product.vn_introduction = res.product.vn_introduction
              data.product.vn_description = res.product.vn_description
            }
        })

        return true
    }
});


new Vue({
    el: '#app',
    data: data,
    ready: function () {
    },
    methods: {
      send: function () {
        this.isSaving = true
        chrome.runtime.sendMessage({
            from: 'iframe',
            subject: 'send',
            data: data.product
        }, (res) => {
            if (res.error) {
              alert('Error occurs, please contact technical for details')
              this.isSaving = false
            } else {
              this.isSaving = false
              parent.postMessage('closeIframe', '*')
            }

        })
      },
      close: function (e) {
        parent.postMessage('closeIframe', '*')
      }
    }
})
