from django.core.management.base import BaseCommand, CommandError

from webapp.models import *
from translatex.models import *
from difflib import SequenceMatcher


def similar(a, b):
    return SequenceMatcher(None, a, b).ratio()

class Command(BaseCommand):
    help = 'Find similars products'

    def handle(self, *args, **options):

        for item in ProductTranslation.objects.exclude(
            amazon_product__ean='').filter(
            amazon_product__ean__isnull=False).order_by('-id'):
            print 'Checking: %s...' % item.amazon_product.title[:10]

            similars = list()

            for pd in ProductTranslation.objects.exclude(amazon_product__ean='').filter(amazon_product__ean__isnull=False):
                if item == pd:
                    continue

                similar_rate = similar(item.amazon_product.title, pd.amazon_product.title)

                if similar_rate > 0.55:
                    similars.append({
                        'ean': pd.amazon_product.ean,
                        'rate': similar_rate
                    })

            if len(similars) > 0:
                self.stdout.write(self.style.SUCCESS('Found: %s' % len(similars)))
                similars = sorted(similars, key=lambda k: k['rate'], reverse=True)
                item.similar_eans = ','.join([i['ean'] for i in similars])
                item.save()

        self.stdout.write(self.style.SUCCESS('Done!'))
