import math



def roundup(x):
    return int(math.ceil(x / 1000.0)) * 1000


print roundup(91800)
print roundup(111111)
print roundup(222222)
print roundup(333333)
