# -*- coding: utf-8 -*-
from django import forms
from django.conf import settings

class RequestMemberShipForm(forms.Form):
    email = forms.EmailField(
        label='Email',
        required=True,
        widget=forms.TextInput(attrs={'class': 'form-control'}),
    )
    message = forms.CharField(
        required=True,
        label='Tin nhắn',
        widget=forms.Textarea(attrs={'class': 'form-control'})
    )


class ContactForm(forms.Form):
    email = forms.EmailField(label='Email', required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    name = forms.CharField(max_length=200, required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    message = forms.CharField(required=True, widget=forms.Textarea(attrs={'class': 'form-control'}))


class ProductRequestForm(forms.Form):
    email = forms.EmailField(label='Email', required=False, widget=forms.TextInput(attrs={'class': 'form-control'}))
    content = forms.CharField(required=True, widget=forms.Textarea(attrs={'class': 'form-control'}))


class OrderMessageForm(forms.Form):
    message = forms.CharField(
        required=True,
        widget=forms.Textarea(attrs={'class': 'form-control', 'rows': 3}))


class ShippingAddressForm(forms.Form):
    shipping_fullname = forms.CharField(
        min_length=5,
        max_length=20,
        required=True,
        label='Tên người nhận',
        widget=forms.TextInput(attrs={'class': 'form-control medium', 'placeholder': 'Nguyen Van A'}))
    shipping_phone = forms.CharField(
        min_length=8,
        max_length=15,
        required=True,
        label='Số điện thoại',
        widget=forms.TextInput(attrs={'class': 'form-control medium', 'placeholder': '098 000 0000'}))

    shipping_prefecture = forms.ChoiceField(
        required=True,
        label='Tỉnh / Thành phố',
        choices=settings.PROVINCES,
        widget=forms.Select(attrs={'class': 'form-control'}))

    shipping_address1 = forms.CharField(
        max_length=200,
        required=True,
        label='Quận/Huyện',
        widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Tên Quận Huyện'}))

    shipping_address2 = forms.CharField(
        max_length=200,
        required=True,
        label='Địa chỉ',
        widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Số nhà, đường, phố...'}))
