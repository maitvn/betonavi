# -*- coding: utf-8 -*-
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from webapp.models import *
from .serializers import *
from amazon.api import AmazonAPI
from django.conf import settings
from .models import *
import json
import time
from django.db.models import Q
from .decorators import api_common


@csrf_exempt
@api_view(['GET', 'POST'])
def barcode_search(request):
    barcode = request.GET.get('barcode', None)

    if not barcode:
        return JsonResponse({
            'message': 'barcode not specified',
            'status': 101,
        }, status=400)


    amazon_product = AmazonProduct.objects.filter(ean=barcode).first()
    if not amazon_product:
        try_time = 0
        while True:
            try:
                amazon = AmazonAPI(
                    settings.AMAZON_ACCESS_KEY,
                    settings.AMAZON_ACCESS_SECRET,
                    settings.AMAZON_AFFILIATE_ID,
                    region=settings.AMAZON_REGION)

                products = amazon.search(Keywords=barcode, SearchIndex='All')

                amazon_product = None
                for i, product in enumerate(products):
                    amazon_product = product
                    break

                jp_price = None
                try:
                    jp_price = int(amazon_product.price_and_currency[0])
                except Exception:
                    pass

                images = []
                try:
                    for img in amazon_product.images:
                        images.append(str(img.LargeImage.URL))
                except Exception:
                    pass

                reviews_url = ''
                try:
                    reviews_url = amazon_product.reviews[1]
                except Exception:
                    pass

                amazon_product = AmazonProduct.objects.create(
                    title=amazon_product.title,
                    ean=amazon_product.ean,
                    asin=amazon_product.asin,
                    brand=amazon_product.brand,
                    manufacturer=amazon_product.manufacturer,
                    publisher=amazon_product.publisher,
                    features=json.dumps(amazon_product.features),
                    product_group=amazon_product.product_group,
                    product_type_name=amazon_product.product_type_name,
                    sales_rank=amazon_product.sales_rank,
                    large_image_url=amazon_product.large_image_url,
                    medium_image_url=amazon_product.medium_image_url,
                    price=jp_price,
                    reviews_url=reviews_url,
                    images_list=json.dumps(images),
                    is_scanned=True,
                    scanned_count=1
                )

                # Quit loop
                break
            except Exception as e:
                try_time += 1
                if try_time < 5:
                    time.sleep(1)
                    continue

                return JsonResponse({
                    'message': 'search error',
                    'status': 100,
                    'try_time': try_time,
                    'detail': str(e)
                })
    else:
        amazon_product.scanned_count += 1
        amazon_product.save()

    return JsonResponse({
        'status': 200,
        'message': 'success',
        'name': 'barcode_search',
        'amazon_product': AmazonProductSerializer(
            amazon_product,
            many=False,
            context={'request': request}
        ).data
    })


@csrf_exempt
@api_view(['GET', 'POST'])
@api_common
def home(request):
    top_scanned_products = AmazonListProductSerializer(
        AmazonProduct.objects.filter(
            ean__isnull=False,
            product__isnull=False,
            medium_image_url__isnull=False
        ).exclude(
            medium_image_url=''
        ).order_by('-scanned_count').select_related('product', 'product__category')[:20],
        many=True,
        context={'request': request}
    ).data

    hot_products = AmazonListProductSerializer(
        AmazonProduct.objects.filter(
            ean__isnull=False,
            product__isnull=False,
            medium_image_url__isnull=False
        ).exclude(
            medium_image_url=''
        ).order_by('?').select_related('product', 'product__category')[:20],
        many=True,
        context={'request': request}
    ).data

    hot_beauty_products = AmazonListProductSerializer(
        AmazonProduct.objects.filter(
            ean__isnull=False,
            product__isnull=False,
            medium_image_url__isnull=False,
            product_type_name='BEAUTY'
        ).exclude(
            medium_image_url=''
        ).order_by('?').select_related('product', 'product__category')[:20],
        many=True,
        context={'request': request}
    ).data

    hot_home_products = AmazonListProductSerializer(
        AmazonProduct.objects.filter(
            ean__isnull=False,
            product__isnull=False,
            medium_image_url__isnull=False,
            product_type_name='KITCHEN'
        ).exclude(
            medium_image_url=''
        ).order_by('?').select_related('product', 'product__category')[:20],
        many=True,
        context={'request': request}
    ).data

    health_products = AmazonListProductSerializer(
        AmazonProduct.objects.filter(
            ean__isnull=False,
            product__isnull=False,
            medium_image_url__isnull=False,
            product_type_name='HEALTH_PERSONAL_CARE'
        ).exclude(
            medium_image_url=''
        ).order_by('?').select_related('product', 'product__category')[:20],
        many=True,
        context={'request': request}
    ).data

    baby_products = AmazonListProductSerializer(
        AmazonProduct.objects.filter(
            ean__isnull=False,
            product__isnull=False,
            medium_image_url__isnull=False,
            product_type_name='BABY_PRODUCT'
        ).exclude(
            medium_image_url=''
        ).order_by('?').select_related('product', 'product__category')[:20],
        many=True,
        context={'request': request}
    ).data

    return JsonResponse({
        'message': 'success',
        'name': 'ranking_list',
        'data': [
            {
                'title': u'Scan nhiều nhất',
                'products': top_scanned_products
            },
            {
                'title': u'Sản phẩm đang HOT',
                'products': hot_products
            },
            {
                'title': u'Mỹ phẩm làm đẹp',
                'products': hot_beauty_products
            },
            {
                'title': u'Sản phẩm cho gia đình',
                'products': hot_home_products
            },
            {
                'title': u'Sức khoẻ Y Tế',
                'products': health_products
            },
            {
                'title': u'Chăm sóc em bé',
                'products': baby_products
            },

        ]
    })


@csrf_exempt
@api_view(['GET', 'POST'])
def search(request):
    keyword = request.GET.get('keyword', None)
    if not keyword:
        keyword = request.POST.get('keyword', None)
    product_list = []
    if keyword:
        keyword_object = SearchKeyword.objects.filter(keyword=keyword).first()
        if keyword_object:
            keyword_object.count += 1
            keyword_object.save()
        else:
            SearchKeyword.objects.create(
                keyword=keyword,
                is_from_mobile=True
            )
        product_list = AmazonListProductSerializer(
            AmazonProduct.objects.filter(
                Q(
                    Q(title__icontains=keyword) |
                    Q(product__vn_name__icontains=keyword)
                ),
                ean__isnull=False,
                medium_image_url__isnull=False
            ).exclude(
                medium_image_url=''
            ).select_related('product', 'product__category')[:20],
            many=True,
            context={'request': request}
        ).data

    return JsonResponse({
        'message': 'success',
        'name': 'search api',
        'product_list': product_list
    })


@csrf_exempt
@api_view(['GET', 'POST'])
def favorite(request):
    product_list = ProductSerializer(
        Product.objects.all().order_by('-view_count')[:20],
        many=True,
        context={'request': request}
    ).data

    return JsonResponse({
        'message': 'success',
        'name': 'ranking_list',
        'products': product_list
    })


@csrf_exempt
@api_view(['GET', 'POST'])
def device(request):
    product_list = ProductSerializer(
        Product.objects.all().order_by('-view_count')[:20],
        many=True,
        context={'request': request}
    ).data

    return JsonResponse({
        'message': 'success',
        'name': 'ranking_list',
        'products': product_list
    })
