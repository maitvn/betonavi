# -*- coding: utf-8 -*-
from django.core.mail import send_mail
from .models import *
from django.conf import settings
from .constants import get_status_display
from django.core.mail import EmailMessage


def mail_change_assignee(product, action_user):
    mail_tos = [product.assignee.user.email]
    email = EmailMessage(
        u'[betonavi][PTP] #%s  Yêu cầu dịch sản phẩm - %s' % (
            product.id,
            product.amazon_product.title[:15]
        ),
u"""

Hi %s,

Bạn đã được giao đảm nhận sản phẩm mới.

Xem chi tiết sản phẩm tại link dưới đây:
https://betonavi.com/translatex/sp/%s


Người phụ trách review: %s


-
Do not reply this email due to automatically sent!

""" % (
product.assignee.fullname if product.assignee else '-',
product.id,
action_user.username
),
        'PTP-betonavi@betonavi.com',
        mail_tos,
        settings.ADMIN_EMAILS
    )

    email.send(fail_silently=False)


def mail_status_change(product, action_user, old_status):
    mail_tos = []
    if product.assignee and product.assignee.user.email:
        mail_tos.append(product.assignee.user.email)
    else:
        mail_tos = ['longtm.it@gmail.com']

    email = EmailMessage(
        u'[betonavi][PTP] [%s] #%s  Trạng thái thay đổi [%s -> %s] - %s' % (
            action_user.username,
            product.id,
            get_status_display(old_status),
            get_status_display(int(product.status)),
            product.amazon_product.title[:15]
        ),
u"""


Trạng thái thay đổi: [%s -> %s]
Assignee: %s

Updated by: %s


link: https://betonavi.com/translatex/sp/%s



-
Do not reply this email due to automatically sent!

""" % (
get_status_display(old_status),
get_status_display(int(product.status)),
product.assignee.fullname if product.assignee else '-',
action_user.username,
product.id
),
        'PTP-betonavi@betonavi.com',
        mail_tos,
        settings.ADMIN_EMAILS
    )
    email.send(fail_silently=False)


def mail_comment(product, action_user, comment):
    mail_tos = []

    if product.assignee and product.assignee.user.email:
        mail_tos.append(product.assignee.user.email)
    else:
        mail_tos = ['longtm.it@gmail.com']

    email = EmailMessage(
        u'[betonavi][PTP] #%s  Bình luận mới: %s' % (product.id, product.amazon_product.title[:15]),
u"""

Product: %s
Assignee: %s

Comment:
-------------------
%s
-------------------
Comment by: %s



link: https://betonavi.com/translatex/sp/%s


-
Do not reply this email due to automatically sent!

""" % (
    product.amazon_product.title,
    product.assignee.fullname if product.assignee else '-',
    comment,
    action_user.username,
    product.id
    ),
        'PTP-betonavi@betonavi.com',
        mail_tos,
        settings.ADMIN_EMAILS
    )

    email.send(fail_silently=False)
