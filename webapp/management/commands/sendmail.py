# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from webapp.models import *
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
# from django.template import Context
from webapp.mailutil import EmailUtil
from django.conf import settings
from django.core.mail import mail_admins

class Command(BaseCommand):
    help = 'Send emails batch'

    def handle(self, *args, **options):
        mailutil = EmailUtil()

        mailutil.mail_magazine()


        # Order emails
        for order in Order.objects.all():
            params = {
                'username': order.user.username,
                'order_detail': 'https://betonavi.com/order/detail/%s' % order.uuid,
                'order_number': order.order_number
            }
            if not order.sent_order_confirm_mail:
                mailutil.send_order_confirm(
                    params=params,
                    mail_to=order.user.email,
                )
                order.sent_order_confirm_mail = True
                order.save()

            if not order.sent_order_delivering_mail and order.status == settings.ORDER_STATUS_PAID:
                mailutil.send_order_delivering(
                    params=params,
                    mail_to=order.user.email,
                )
                order.sent_order_delivering_mail = True
                order.save()

            if not order.sent_order_complete_mail and order.status == settings.ORDER_STATUS_COMPLETED:
                mailutil.send_order_complete(
                    params=params,
                    mail_to=order.user.email,
                )
                order.sent_order_complete_mail = True
                order.save()


        for item in OrderMessage.objects.filter(is_mail_user=False):
            item.is_mail_user = True
            item.save()
            params = {
                'username': item.order.user.username,
                'order_detail': 'https://betonavi.com/order/detail/%s' % item.order.uuid,
                'order_number': item.order.order_number,
                'message': item.message
            }
            mailutil.send_order_message(
                params=params,
                mail_to=item.order.user.email,
            )



        for item in RequestInvitation.objects.filter(is_notify_admin=False):
            mail_admins(
                u'[RequestInvitation] Yêu cầu làm thành viên betonavi từ %s' % item.email,
                u'''
                    Hi Admins,

                    %s đã gửi yêu cầu tạo tài khoản.

                    Tin nhắn là
                    %s
                ''' % (item.email, item.message),
            )
            item.is_notify_admin = True
            item.save()

        for item in RequestInvitation.objects.filter(
            is_ready_send_reply=True,
            is_reply=False,
            reply_message__isnull=False
            ):

            html_message = "<br />".join(item.reply_message.replace("\r", "\n").split("\n"))

            mailutil.send_reply_invite_request(
                params={
                    'message': item.reply_message,
                },
                html_params={
                    'message': html_message,
                },
                mail_to=item.email,
            )
            item.is_reply = True
            item.save()
