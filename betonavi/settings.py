"""
Django settings for betonavi project.

Generated by 'django-admin startproject' using Django 1.10.3.

For more information on this file, see
https://docs.djangoproject.com/en/1.10/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.10/ref/settings/
"""

import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.10/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '=-ss2rio@)w%wufs_sb3aic&c9*ns!iw7s#x+k3hs95*1vu12c'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = [
    '*',
    'betonavi.com'
]

HOST = 'https://betonavi.com'

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'rest_framework',
    'django.contrib.sites',
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    # 'tinymce',

    'debug_toolbar',
    'django_user_agents',
    'django.contrib.humanize',

    'webapp.apps.WebappConfig',
    'mobile.apps.MobileConfig',
    'translatex.apps.TranslatexConfig',
    'invitations',

    'markdownx',
    "anymail",

]

SITE_ID = 1
USER_AGENTS_CACHE = 'default'


MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',

    'debug_toolbar.middleware.DebugToolbarMiddleware',
    'webapp.middleware.ec_middleware.ECMiddleware',
]


ROOT_URLCONF = 'betonavi.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

INTERNAL_IPS = ['127.0.0.11', ]


WSGI_APPLICATION = 'betonavi.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'betonavi',
        'USER': 'root',
        'PASSWORD': 'root',
        'HOST': 'localhost',
        'PORT': '3306',
    }
}


# Password validation
# https://docs.djangoproject.com/en/1.10/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/1.10/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Asia/Tokyo'

USE_I18N = True

USE_L10N = True

USE_TZ = False


# PTP maintenance is true, editors can not edit or translation product
PTP_MAINTENANCE = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.10/howto/static-files/
if not DEBUG:
    MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
else:
    MEDIA_ROOT = os.path.join(BASE_DIR, 'dev_media')

MEDIA_URL = '/media/'
STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, "static")

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "resources/assets/"),
    # '/var/www/static/',
]

# DATETIME_FORMAT = "Y-m-d H:i:s"



# -- mailgun settings --#
# EMAIL_HOST = 'smtp.mailgun.org'
# EMAIL_HOST_USER = 'postmaster@betonavi.com'
# EMAIL_HOST_PASSWORD = 'd0407a5b628cd5e1491344d56d621611'
# EMAIL_TIMEOUT = 5

ANYMAIL = {
    # (exact settings here depend on your ESP...)
    "MAILGUN_API_KEY": "key-f68e698f09ed7462fdbdf170f1c05793",
    "MAILGUN_SENDER_DOMAIN": 'betonavi.com',  # your Mailgun domain, if needed
}
EMAIL_BACKEND = "anymail.backends.mailgun.EmailBackend"  # or sendgrid.EmailBackend, or...



CONTACT_EMAIL = 'lienhe@betonavi.com'
SUPPORT_EMAIL = 'hotrokhachhang@betonavi.com'
DEFAULT_FROM_EMAIL = 'admin@betonavi.com'
ORDER_FROM_EMAIL = 'order@betonavi.com'
MAGAZINE_FROM_EMAIL = 'betonavi-info@betonavi.com'
ADMINS = [
    ('Long Tran', 'longtm.it@gmail.com'),
    # ('Hung Pham', 'duyhungp@gmail.com'),
    # ('Vu Hoai', 'dustilnguyen0211@gmail.com'),
]

ADMIN_EMAILS = [
    'longtm.it@gmail.com',
    'duyhungp@gmail.com',
    'dustilnguyen0211@gmail.com',
    'hungbknv@gmail.com',
    'tranhoangbg@gmail.com',
    'anhntn229@gmail.com',
]

EMAIL_SUBJECT_PREFIX = '[Betonavi]'

# # TinyMCE config
# TINYMCE_DEFAULT_CONFIG = {
#     'theme': "advanced",
#     # 'plugins': "spellchecker",
#     # 'theme_advanced_buttons3_add': "|,spellchecker",
# }

ACCOUNT_EMAIL_REQUIRED = True


# ACCOUNT_AUTHENTICATION_METHOD = 'email'
ACCOUNT_ADAPTER = 'invitations.models.InvitationsAdapter'
# INVITATIONS_INVITATION_ONLY = True
INVITATIONS_ACCEPT_INVITE_AFTER_SIGNUP = False
INVITATIONS_GONE_ON_ACCEPT_ERROR = False
INVITATIONS_INVITATION_EXPIRY = 30


AMAZON_ACCESS_KEY = 'AKIAIXC5CY6IM4SMKY6A'
AMAZON_ACCESS_SECRET = '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'
AMAZON_AFFILIATE_ID = 'ryu321-22'
AMAZON_REGION = 'JP'

CLIENT_KEY = '=-ss2rio@)w%wufs_sb3aic&c9*ns!iw7s#x+k3hs95*1vu12c'

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
    }
}

# Including local configurations
try:
    from constants import *
except ImportError as e:
    pass


# Including local configurations
try:
    from local_settings import *
except ImportError as e:
    pass
