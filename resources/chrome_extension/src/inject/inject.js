function extractDetailTable () {
	var detailTableArr = {};

	var detailTable1 = document.getElementById('prodDetails')
	detailTable1 && detailTable1.querySelectorAll('table').forEach((value, index) => {
		value.querySelectorAll('tr').forEach((tr, index) => {
			var tds = tr.querySelectorAll('td')
			if (tds.length === 2 && tds[0].innerText.trim()) {
				detailTableArr[tds[0].textContent.trim()] = tds[1].textContent.trim().replace(/\n\s*\n/g, '\n')
			}
		})
	})

	var detailTable2 = document.getElementById('detail_bullets_id')
	detailTable2 && detailTable2.querySelectorAll('li').forEach((item, index) => {
		var textContent = item.innerText.trim()
		var keyValue = textContent.split(':')

		if (keyValue.length === 2) {
			var key = keyValue[0].trim()
			var value = keyValue[1].trim().replace(/\n\s*\n/g, '\n')
			detailTableArr[key] = value;
		}
	})

	return detailTableArr;
}

function extractRawRating (detailTable) {
	const similarKeys = ['Average Customer Review']
	for (var key in detailTable) {
	  if (similarKeys.indexOf(key) >= 0) {
			return detailTable[key]
		}
	}
	return '--'
}

function extractRawWeight (detailTable) {
	const similarKeys = ['Shipping Weight']
	for (var key in detailTable) {
	  if (similarKeys.indexOf(key) >= 0) {
			return detailTable[key]
		}
	}
	return '--'
}


chrome.extension.sendMessage({}, function(response) {
	var readyStateCheckInterval = setInterval(function() {
	if (document.readyState === "complete") {
		clearInterval(readyStateCheckInterval);

		// ----------------------------------------------------------
		// This part of the script triggers when page is done loading
		console.log("Hello. This message was sent from scripts/inject.js");
		// ----------------------------------------------------------


		const betonaviBtn = document.createElement('div')
		betonaviBtn.setAttribute('id', 'btnv-main-btn')
		betonaviBtn.appendChild(document.createTextNode("BetoNavi"));
		document.body.appendChild(betonaviBtn);

		betonaviBtn.addEventListener("click", function(){
				var productInfo = {}

				var price = document.getElementById('priceblock_ourprice') || document.getElementById('priceblock_saleprice')
				if (price) {
					productInfo.jp_price = price.textContent.trim()
				}else {
					productInfo.jp_price = '0'
				}

				const name = document.getElementById('productTitle')

				productInfo.jp_name = name.textContent.trim()

				var description = document.getElementById('productDescription')
				if (description) {
					const descChild = description.querySelector('#productDescription')
					if (descChild) {
						description = descChild
					}
					productInfo.jp_description = description.textContent.trim().replace(/\n\s*\n/g, '\n')
				}

				// edited
				var detailTableArr = extractDetailTable()
				productInfo.jp_detail_table = JSON.stringify(detailTableArr)

				if ('ASIN' in detailTableArr) {
					productInfo.asin = detailTableArr['ASIN']
				} else {
					var m = location.href.match(/\/dp\/([A-Z0-9]+)[^A-Z0-9]*/i)
					if (m && m.length > 1) {
						productInfo.asin = m[1]
					} else {
						m = location.href.match(/\/gp\/product\/([A-Z0-9]+)[^A-Z0-9]*/i)
						if (m && m.length > 1) {
							productInfo.asin = m[1]
						}
					}
				}

				var images = []
				const imgWrappers = document.getElementsByClassName("a-spacing-small item")
				Array.prototype.forEach.call(imgWrappers, function(item) {
					const img = item.querySelectorAll('img')[0]

					var re = /^(https.*\._)(.*)\.(jpg|png)$/i
					var found = img.src.match(re)
					if (found) {
						const processedImage = `${found[1]}S500_.${found[3]}`
						images.push(processedImage)
					}
				})
				productInfo.images = images

        document.getElementById("btnv-popup").style.display = "block";
        chrome.runtime.sendMessage({
            from: 'inject',
            subject: 'openPopup',
            data: productInfo
        });
				console.log('Ahahaha, hahah')
    });

	}
	}, 10);
});

// Avoid recursive frame insertion...
var extensionOrigin = 'chrome-extension://' + chrome.runtime.id;
if (!location.ancestorOrigins.contains(extensionOrigin)) {
    var iframe = document.createElement('iframe');
    iframe.setAttribute('id', 'btnv-popup')
    // Must be declared at web_accessible_resources in manifest.json
    iframe.src = chrome.runtime.getURL('/src/inject/frame.html');

    // Some styles for a fancy sidebar
    iframe.style.cssText = `
        display: none;
        position: fixed;
        bottom: 0px;
        right: 10px;
        width: 600px;
        height: 600px;
        opacity: 1 !important;
        z-index: 9999999999;

    		border-width: 1px 1px 0 1px;
				border-style: solid;
				border-color:  #795548;
    `;

    document.body.appendChild(iframe);
}


addEventListener('message', function(ev) {
    if (ev.data === 'closeIframe') {
        document.getElementById("btnv-popup").style.display = "none";
    }
});


(function createStyle () {
    var css = `
        #btnv-main-btn {
            cursor: pointer;
            position: fixed;
            bottom: 40px;
            right: 40px;
            border-radius: 20px;
            display: inline-block;
            color: #fff;
            line-height: 24px;
            padding: 3px 24px;
            text-align: center;
            font-size: 16px;
            background-color: #ed4224;
            -webkit-transition: .4s; /* Safari */
            transition: .4s;
						z-index: 999999999;
         }
        #btnv-main-btn:hover{

         }
    `,
    head = document.head || document.getElementsByTagName('head')[0],
    style = document.createElement('style');

    style.type = 'text/css';
    if (style.styleSheet){
      style.styleSheet.cssText = css;
    } else {
      style.appendChild(document.createTextNode(css));
    }
    head.appendChild(style);
})();
