# -*- coding: utf-8 -*-
from django import forms
from django.conf import settings
from .constants import TICKET_STATUS_OPTIONS, TICKET_REVIEW_OPTIONS, TRANSLATOR_STATUS_OPTIONS
from .models import *
from django.contrib.auth.models import User


class TicketStatusForm(forms.Form):
    status = forms.ChoiceField(
        required=True,
        label='Trạng thái',
        choices=TRANSLATOR_STATUS_OPTIONS,
        widget=forms.Select(attrs={'class': 'form-control'}))


class ReviewForm(forms.ModelForm):
    class Meta:
        model = ProductTranslation
        fields = ['assignee', 'status', 'rating', 'charge_amount' ]

class ContentForm(forms.ModelForm):
    class Meta:
        model = ProductTranslation
        fields = ['vn_name', 'vn_introduction', 'vn_description', 'vn_usage', 'vn_notice' ]

class EditTicketForm(forms.ModelForm):
    class Meta:
        model = ProductTranslation
        fields = ['reference_url1',
            'translated_time',
            'reference_url2',
            'translated_rate',
            'reference_url3',
         ]


class TranslationCommentForm(forms.Form):
    message = forms.CharField(
        required=True,
        widget=forms.Textarea(attrs={
            'class': 'form-control',
            'rows': 6,
            'cols': 80
            }
    ))


class TranslatorForm(forms.ModelForm):
    user = forms.ModelChoiceField(queryset=User.objects.all(),
                                  widget=forms.TextInput)
    class Meta:
        model = Translator
        fields = ['user', 'fullname', 'bank_account_info', ]
