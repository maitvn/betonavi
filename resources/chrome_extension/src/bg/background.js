// if you checked "fancy-settings" in extensionizr.com, uncomment this lines

// var settings = new Store("settings", {
//     "sample_setting": "This is how you use Store.js to remember values"
// });
const SERVER = 'https://betonavi.com/';
// const SERVER = 'http://localhost:8004/';

//example of using a message handler from the inject scripts
// chrome.extension.onMessage.addListener(
//   function(request, sender, sendResponse) {
//   	// chrome.pageAction.show(sender.tab.id);
//     sendResponse();
//   }
// );

chrome.runtime.onMessage.addListener(function (msg, sender, response) {
    if ((msg.from === 'inject') && (msg.subject === 'openPopup')) {
        /* Enable the page-action for the requesting tab */

        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
          chrome.tabs.sendMessage(tabs[0].id, {
            from: "background",
            subject: "openPopup",
            data: msg.data
          });
        });
        return true
    }

    if ((msg.from === 'iframe') && (msg.subject === 'send')) {
        request('POST', 'ext/api/import_from_amazon', msg.data, function (res) {
            response(res)
        }, function (err) {
            response({error: true})
        });
        return true;
    }

    if ((msg.from === 'iframe') && (msg.subject === 'category')) {
        request('GET', 'ext/api/categories', {}, function (res) {
            response(res)
        });
        return true;
    }

    if ((msg.from === 'iframe') && (msg.subject === 'check')) {
        request('GET', 'ext/api/product', msg.data, function (res) {
            response(res)
        });
        return true;
    }
});


function request (method, url, data, successCb, errorCb) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == XMLHttpRequest.DONE ) {
         if (xmlhttp.status === 200) {
             try {
                  var response = JSON.parse(xmlhttp.responseText);
                  successCb && successCb(response);
              }
              catch(err) {
                errorCb && errorCb();
              }
         } else if (xmlhttp.status === 310) {
            chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
              chrome.tabs.sendMessage(tabs[0].id, {
                from: "background",
                subject: "auth_required",
                data: data
              });
            });
         } else {
           errorCb && errorCb();
         }
      }
    };

    var str = "";
    for (var key in data) {
        if (str != "") {
            str += "&";
        }
        str += key + "=" + encodeURIComponent(data[key]);
    }
    if (method === 'POST') {
        xmlhttp.open(method, SERVER + url, true);
        xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    } else {
        url = str ? url + '?' + str : url;
        xmlhttp.open(method, SERVER + url, true);
    }
    xmlhttp.send(str);
}
