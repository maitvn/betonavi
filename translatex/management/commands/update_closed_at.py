from django.core.management.base import BaseCommand, CommandError

from webapp.models import *
from translatex.models import *
from datetime import datetime


class Command(BaseCommand):
    help = 'Calculate Billing monthly'

    def handle(self, *args, **options):

        for item in ProductTranslation.objects.filter(status=4):
            self.stdout.write(self.style.SUCCESS('[Update]... %s' % item.id))
            item.closed_at = item.updated_at
            item.save()

        self.stdout.write(self.style.SUCCESS('Done!'))
