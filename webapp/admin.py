from django.contrib import admin
from django.contrib.admin import AdminSite
from .models import *
# from tinymce.widgets import TinyMCE
from django.utils.html import format_html
# from tinymce.widgets import TinyMCE
from markdownx.widgets import AdminMarkdownxWidget
from django.utils.safestring import  mark_safe
from django.contrib import messages


AdminSite.site_header = 'Betonavi admin'
AdminSite.site_title = 'Betonavi admin'
admin.ModelAdmin.list_per_page = 100


class CategoryInline(admin.TabularInline):
    model = Category


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    inlines = [
        CategoryInline
    ]
    list_display = ('name', 'slug', 'display_image', 'parent', 'display_order', 'status', )
    list_editable = ('display_order', )
    list_filter = ('parent', )

    def display_image(self, obj):
        if obj.image:
            return format_html(u'<img src="/media/{}" style="height:40px" alt="" />', obj.image)
        return '-'
    display_image.short_description = 'Image'



class ProductImageInline(admin.TabularInline):
    model = ProductImage


@admin.register(AmazonProduct)
class AmazonProductAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'display_image',
        'asin',
        'ean',
        'title',
        'price',
        'manufacturer',
        'scanned_count',
        'is_scanned',
        'amazon_fetched_at'
    )
    search_fields = ['title', 'asin', 'ean']
    list_filter = ('product_type_name', )

    def display_image(self, obj):
        if obj.medium_image_url:
            return format_html(u'<img src="{}" style="height:40px" alt="" />', obj.medium_image_url)
        return '-'
    display_image.short_description = 'Image'



@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'display_image',
        'vn_name',
        'category',
        'ean',
        'weight',
        'shipping_weight',
        'japan_shipping_cost',
        'oversea_shipping_cost',
        'amazon_price',
        'jp_price',
        'margin_rate',
        'margin_amount',
        'sale_price',
        'availability',
        'on_sale',
        'added_by',
        'amazon'
    )
    search_fields = ['vn_name', 'asin']
    exclude = (
        'jp_detail_table',
        'vn_detail_table',
        'vn_content',
        'status',
        'crawl_status',
        'view_count',
        'sale_price',
        'slug',
        'asin',
        'margin_amount',
        'oversea_shipping_cost',
    )
    list_editable = ('on_sale',)
    # readonly_fields = ('jp_name',)
    list_filter = ('category', )


    formfield_overrides = {
        models.TextField: {'widget': AdminMarkdownxWidget},
    }

    def display_image(self, obj):
        if obj.image:
            return format_html(u'<img src="/media/{}" style="height:40px" alt="" />', obj.image)
        return '-'
    display_image.short_description = 'Image'

    def amazon(self, obj):
        if obj.asin:
            return format_html('<a target="_blank" href="https://www.amazon.co.jp/dp/{}">link</a>', obj.asin)
        return '-'
    amazon.short_description = 'amazon'
    #
    inlines = [
        ProductImageInline
    ]

    def get_queryset(self, request):
        return super(ProductAdmin, self).get_queryset(request).select_related(
            'category', 'sub_category')


@admin.register(EditProduct)
class EditProductAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'display_image',
        'vn_name',
        'score',
        'weight',
        'rank',
        'jp_price',
        'jp_price_adjusted',
        'japan_shipping_cost',
        'amazon'
    )
    search_fields = ['vn_name', 'asin']

    list_filter = ('category', )

    list_editable = (
        'weight',
        'score',
        'jp_price_adjusted',
        'japan_shipping_cost',
        'rank')

    exclude = (
        'jp_detail_table',
        'vn_detail_table',
        'vn_content',
        'status',
        'crawl_status',
        'view_count',
        'vn_price',
        'sale_price',
        'slug',
        'asin',
        'margin_amount',
        'oversea_shipping_cost',
    )

    formfield_overrides = {
        models.TextField: {'widget': AdminMarkdownxWidget},
    }

    def display_image(self, obj):
        if obj.image:
            return format_html(u'<img src="/media/{}" style="height:40px" alt="" />', obj.image)
        return '-'
    display_image.short_description = 'Image'

    def amazon(self, obj):
        if obj.asin:
            return format_html('<a target="_blank" href="https://www.amazon.co.jp/dp/{}">link</a>', obj.asin)
        return '-'
    amazon.short_description = 'amazon'

    def get_queryset(self, request):
        return super(EditProductAdmin, self).get_queryset(request).select_related(
            'category', 'sub_category')


@admin.register(EditCategoryProduct)
class EditCategoryProductAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'display_image',
        'vn_name',
        'overseashippingfee',
        'category',
    )
    search_fields = ['vn_name', 'asin']
    list_editable = ('category', 'overseashippingfee',)
    list_filter = ('category', )

    def formfield_for_dbfield(self, db_field, **kwargs):
        request = kwargs['request']
        formfield = super(EditCategoryProductAdmin, self).formfield_for_dbfield(db_field, **kwargs)

        if db_field.name == 'category':
            choices = getattr(request, '_category_choices_cache', None)
            if choices is None:
                request._category_choices_cache = choices = list(formfield.choices)
            formfield.choices = choices

        return formfield

    def display_image(self, obj):
        if obj.image:
            return format_html(u'<img src="/media/{}" style="height:40px" alt="" />', obj.image)
        return '-'
    display_image.short_description = 'Image'

    def get_queryset(self, request):
        return super(EditCategoryProductAdmin, self).get_queryset(
        request
        ).select_related('category')



@admin.register(MailMagazineProduct)
class MailMagazineProductAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'display_image',
        'vn_name',
        'category',
    )
    search_fields = [
        'vn_name',
        'asin']

    list_filter = ('category', )

    def display_image(self, obj):
        if obj.image:
            return format_html(u'<img src="/media/{}" style="height:40px" alt="" />', obj.image)
        return '-'
    display_image.short_description = 'Image'

    def get_queryset(self, request):
        return super(MailMagazineProductAdmin, self).get_queryset(
        request
        ).select_related('category')


@admin.register(Banner)
class BannerAdmin(admin.ModelAdmin):
    list_display = ('name', 'display_image', 'caption', 'link', 'display_order')
    list_editable = ('display_order', )
    def display_image(self, obj):
        if obj.image:
            return format_html(u'<img src="/media/{}" style="height:100px" alt="" />', obj.image)
        return '-'
    display_image.short_description = 'Image'


class OrderItemInline(admin.TabularInline):
    model = OrderItem
    readonly_fields = (
        'product',
        'display_product',
        'quantity',
        'vn_item_price',
        'vn_total_price',
        'jp_item_price',
        'jp_total_price',
        'color',
        'size',
    )
    show_change_link = True

    def display_product(self, obj):
        if obj.product.asin:
            return format_html('<a target="_blank" href="https://www.amazon.co.jp/dp/{}">amazon</a>', obj.product.asin)
        return '-'

    display_product.short_description = 'product'

    can_delete = False
    def has_add_permission(self, request):
        return False


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    change_form_template = 'admin/webapp/change_form_order.html'
    # change_list_template = 'admin/backoffice/change_list.html'

    list_display = (
        'order_number',
        'vn_order_amount',
        # 'vn_total_price',
        # 'price_off',
        # 'vn_total_price',
        'jp_total_price',
        'shipping_prefecture',
        'user',
        'ordred_at',
        'sent_order_confirm_mail',
        'sent_order_delivering_mail',
        'sent_order_complete_mail',
        'status',
    )
    readonly_fields = (
        'order_number',
        'vn_order_amount',
        'vn_total_price',
        'shipping_fee',
        'price_off',
        'jp_total_price',
        'total_weight',
        'shipping_prefecture',
        'user',
        'ordred_at',
        'shipping_fullname',
        'shipping_phone',
        'shipping_prefecture',
        'shipping_address1',
        'shipping_address2',
        'shipping_method',
    )
    exclude=(
        'sent_order_confirm_mail',
        'sent_order_delivering_mail',
        'sent_order_complete_mail')

    inlines = [
        OrderItemInline
    ]
    can_delete = False
    # def has_add_permission(self, request):
    #     return False
    # def has_delete_permission(self, request, obj=None):
    #     return False

    def change_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['order_messages'] = OrderMessage.objects.filter(order_id=object_id)
        return super(OrderAdmin, self).change_view(request, object_id, extra_context=extra_context)



@admin.register(OrderMessage)
class OrderMessageAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'order',
        'message',
        'user',
        'staff',
        'status',
    )
    # list_filter = ('order', )


@admin.register(RequestInvitation)
class RequestInvitationAdmin(admin.ModelAdmin):
    list_display = ('id', 'email', 'message', 'is_notify_admin', 'invite_button', 'is_reply', 'created_at'  )
    readonly_fields = ('status', 'is_reply', 'is_notify_admin', )

    def invite_button(self, obj):
        if not obj.is_send_invite:
            return format_html(u'<button class="send-invite-email" data-id="{}">Send Invite</button>', obj.id)
        return '-'
    invite_button.short_description = 'Invite'
    class Media:
        # css = {
        #     "all": ("my_styles.css",)
        # }
        js = ("admin/js/extend.js",)

@admin.register(Option)
class OptionAdmin(admin.ModelAdmin):
    list_display = ('id', 'key', 'number_value', 'text_value',  )


@admin.register(PriceTable)
class PriceTableAdmin(admin.ModelAdmin):
    list_display = ('id', 'min_price', 'max_price', 'rate', 'created_at', 'updated_at'  )

@admin.register(BankAccount)
class BankAccountAdmin(admin.ModelAdmin):
    list_display = ('id', 'bank_name', 'branch_name', 'owner_name', 'number',  )


class SetItemInline(admin.TabularInline):
    model = SetItem
    raw_id_fields = ('product',)

@admin.register(FeatureSet)
class FeatureSetAdmin(admin.ModelAdmin):
    inlines = [
        SetItemInline
    ]

    list_display = ('id', 'display_image', 'name', 'slug', 'description',  )

    def display_image(self, obj):
        if obj.image:
            return format_html(u'<img src="/media/{}" style="height:40px" alt="" />', obj.image)
        return '-'
    display_image.short_description = 'Image'


@admin.register(Coupon)
class CouponAdmin(admin.ModelAdmin):

    list_display = ('id', 'display_image', 'name', 'is_invitation_coupon', 'price_off',  )

    def display_image(self, obj):
        if obj.image:
            return format_html(u'<img src="/media/{}" style="height:40px" alt="" />', obj.image)
        return '-'
    display_image.short_description = 'Image'


@admin.register(ProductRequest)
class ProductRequestAdmin(admin.ModelAdmin):

    list_display = ('id', 'email', 'user', 'content', 'is_notify_admins',  )


@admin.register(MailMagazine)
class MailMagazineAdmin(admin.ModelAdmin):

    list_display = ('id', 'subject', 'is_processed', 'scheduled_at',  )

    filter_horizontal = ('categories', )



@admin.register(OverseaShippingFee)
class OverseaShippingFeeAdmin(admin.ModelAdmin):
    list_display = ('id', 'type', 'price', 'ovt_price', 'jk_price',)
